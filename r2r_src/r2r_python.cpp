#include "r2r.h"
#include "../redner_src/redner.cpp"

PYBIND11_MODULE(r2r, m) {
    m.doc() = "r2rmini"; // optional module docstring
    m.def("compute_num_channels", compute_num_channels, "");
    m.def("r2r_forward_render", &r2r_forward_render, "");
    m.def("r2r_render", &r2r_render, ""); //LOL...
    m.def("r2r_bdpt_render", &r2r_bdpt_render, ""); //LOL...
    // /// Tests
    // m.def("test_sample_primary_rays", &test_sample_primary_rays, "");
    // m.def("test_scene_intersect", &test_scene_intersect, "");
    // m.def("test_sample_point_on_light", &test_sample_point_on_light, "");
    // m.def("test_active_pixels", &test_active_pixels, "");
    // m.def("test_camera_derivatives", &test_camera_derivatives, "");
    // m.def("test_d_bsdf", &test_d_bsdf, "");
    // m.def("test_d_bsdf_sample", &test_d_bsdf_sample, "");
    // m.def("test_d_bsdf_pdf", &test_d_bsdf_pdf, "");
    // m.def("test_d_intersect", &test_d_intersect, "");
    // m.def("test_d_sample_shape", &test_d_sample_shape, "");
}
