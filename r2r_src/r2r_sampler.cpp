#include "r2r_sampler.h"
#include "../redner_src/parallel.h"
#include <thrust/random.h>
#include "../redner_src/pcg_sampler.cpp"

// AVA:
// TODO: 

// def _rd_generate(n, d, seed=0.5):
//     """Generates the first ``n`` points in the ``R_d`` sequence."""
//     # http://extremelearning.com.au/unreasonable-effectiveness-of-quasirandom-sequences/
//     def gamma(d, n_iter=20):
//         """Newton-Raphson-Method to calculate g = phi_d."""
//         x = 1.0
//         for _ in range(n_iter):
//             x -= (x**(d + 1) - x - 1) / ((d + 1) * x**d - 1)
//         return x
//     g = gamma(d)
//     alpha = np.zeros(d)
//     for j in range(d):
//         alpha[j] = (1/g) ** (j + 1) % 1
//     z = np.zeros((n, d))
//     z[0] = (seed + alpha) % 1
//     for i in range(1, n):
//         z[i] = (z[i-1] + alpha) % 1
//     return z

// # Returns the i-th term of the canonical R2 sequence
// # for i = 0,1,2,...
// def r2(i):
//   g = 1.324717957244746 # special math constant
//   a = [1.0/g, 1.0/(g*g) ]
//   return [a[0]*(i+1) %1, a[1]*(1+i) %1]

// Initialize each pixel with a PCG rng with a different stream
struct r2r_pixel_initializer 
{
    //TODO : Permuted with idx ?
    DEVICE void operator()(int idx) 
    {
        //
        r2r_pixel_states[idx].state = 0;
        r2r_pixel_states[idx].inc = 0;
        //next_pcg32(&rng_states[idx]);
        //r2r_pixel_states[idx].state += (0x853c49e6748fea9bULL + seed);
        //next_pcg32(&rng_states[idx]);
    }

    uint64_t seed;
    r2r_pixel_state *r2r_pixel_states;
};

template <int spp>
struct r2r_sampler_float {
    DEVICE void operator()(int idx) {
        if(spp == 2){
             const float g = 1.324717957244746; //# special math constant   
             const float a1 = 1.0 / g;
             const float a2 = 1.0 / (g*g);
             int i = r2r_pixel_states[idx].inc++;
            samples[2*idx] = a1 * (i+1) - floor(a1 * (i+1));// % 1;
            samples[2*idx + 1] = a2 *(1+i) - floor(a2 *(1+i));// % 1;
        }
        else {
            for (int i = 0; i < spp; i++) {
                samples[spp * idx + i] = 0.5;
            }
        }
    }
    r2r_pixel_state *r2r_pixel_states;
    float *samples;
};

template <int spp>
struct r2r_sampler_double {
    DEVICE void operator()(int idx) {
        if(spp == 2){
             const double g = 1.324717957244746; //# special math constant   
             const double a1 = 1.0 / g;
             const double a2 = 1.0 / (g*g);
             int i = r2r_pixel_states[idx].inc++;
            samples[2*idx] = a1 * (i+1) - floor(a1 * (i+1));// % 1;
            samples[2*idx + 1] = a2 *(1+i) - floor(a2 *(1+i));// % 1;
        }
        else {
            for (int i = 0; i < spp; i++){
                samples[spp * idx + i] = 0.5;
            }
        }
    }

    r2r_pixel_state *r2r_pixel_states;
    double *samples;
};


r2rSampler::r2rSampler(bool use_gpu,
                 uint64_t seed,
                 int num_pixels) : use_gpu(use_gpu) 
{
    rng_states = Buffer<pcg32_state>(use_gpu, num_pixels);
    parallel_for(pcg_initializer{seed, rng_states.begin()},
        rng_states.size(), use_gpu);

    // First, Let's init the r2r_sampler_state 
    // using number of pixels
    r2r_pixel_states = Buffer<r2r_pixel_state>(use_gpu, num_pixels);
    parallel_for(r2r_pixel_initializer{seed, r2r_pixel_states.begin()},
        r2r_pixel_states.size(), use_gpu);
}

void r2rSampler::next_camera_samples(BufferView<TCameraSample<float>> samples) {
//    parallel_for(pcg_sampler_float<2>{rng_states.begin(),
//       (float*)samples.begin()}, samples.size(), use_gpu);
    parallel_for(r2r_sampler_float<2>{r2r_pixel_states.begin(),
        (float*)samples.begin()}, samples.size(), use_gpu);
}


void r2rSampler::next_camera_samples(BufferView<TCameraSample<double>> samples) {
//    parallel_for(pcg_sampler_double<2>{rng_states.begin(),
//        (double*)samples.begin()}, samples.size(), use_gpu);
    parallel_for(r2r_sampler_double<2>{r2r_pixel_states.begin(),
        (double*)samples.begin()}, samples.size(), use_gpu);

}

void r2rSampler::next_light_samples(BufferView<TLightSample<float>> samples) {
    parallel_for(pcg_sampler_float<4>{rng_states.begin(),
        (float*)samples.begin()}, samples.size(), use_gpu);
}

void r2rSampler::next_light_samples(BufferView<TLightSample<double>> samples) {
    parallel_for(pcg_sampler_double<4>{rng_states.begin(),
        (double*)samples.begin()}, samples.size(), use_gpu);
}

void r2rSampler::next_bsdf_samples(BufferView<TBSDFSample<float>> samples) {
    parallel_for(pcg_sampler_float<3>{rng_states.begin(),
        (float*)samples.begin()}, samples.size(), use_gpu);
}

void r2rSampler::next_bsdf_samples(BufferView<TBSDFSample<double>> samples) {
    parallel_for(pcg_sampler_double<3>{rng_states.begin(),
        (double*)samples.begin()}, samples.size(), use_gpu);
}

void r2rSampler::next_primary_edge_samples(
        BufferView<TPrimaryEdgeSample<float>> samples) {
    parallel_for(pcg_sampler_float<2>{rng_states.begin(),
        (float*)samples.begin()}, samples.size(), use_gpu);
}

void r2rSampler::next_primary_edge_samples(
        BufferView<TPrimaryEdgeSample<double>> samples) {
    parallel_for(pcg_sampler_double<2>{rng_states.begin(),
        (double*)samples.begin()}, samples.size(), use_gpu);
}

void r2rSampler::next_secondary_edge_samples(
        BufferView<TSecondaryEdgeSample<float>> samples) {
    parallel_for(pcg_sampler_float<4>{rng_states.begin(),
        (float*)samples.begin()}, samples.size(), use_gpu);
}

void r2rSampler::next_secondary_edge_samples(
        BufferView<TSecondaryEdgeSample<double>> samples) {
	parallel_for(pcg_sampler_double<4>{rng_states.begin(),
        (double*)samples.begin()}, samples.size(), use_gpu);
}
