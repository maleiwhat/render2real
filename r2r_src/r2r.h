#pragma once

#include "../redner_src/redner.h"
#include "../redner_src/ptr.h"
#include "../redner_src/channels.h"
#include "../redner_src/pathtracer.h"
#include <memory>
#include <cstdio>


int r2r_program_version();

struct Scene;
struct DScene;
struct RenderOptions;

namespace r2r
{
    enum class SamplerType {
	    independent,
	    sobol,
        r2
    };
}

void r2r_render(const Scene &scene,
            const RenderOptions &options,
            ptr<float> rendered_image,
            ptr<float> d_rendered_image,
            std::shared_ptr<DScene> d_scene,
            ptr<float> debug_image,
            ptr<float> input_density_image);

/*    
    Forward render, no gradient calculation, for experiments.
    Currently, we are using the same parameters as the original render. 
    scene, 
    options,
    rendered_image,
    d_scene,   // Not used.
    debug_image
*/

void r2r_forward_render(const Scene &scene,
            const RenderOptions &options,
            ptr<float> rendered_image,
            ptr<float> d_rendered_image,
            std::shared_ptr<DScene> d_scene,
            ptr<float> debug_image,
            ptr<float> input_density_image);


void r2r_bdpt_render(const Scene &scene,
	const RenderOptions &options,
	ptr<float> rendered_image,
	ptr<float> d_rendered_image,
	std::shared_ptr<DScene> d_scene,
	ptr<float> debug_image,
	ptr<float> input_density_image);