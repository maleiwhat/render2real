//#include "thrust_utils.h"
//#include "pathtracer.cpp"

#include "../redner_src/thrust_utils.h"
#include "../redner_src/pathtracer.cpp"
#include "r2r.h"
//#include "r2r_sampler.h"
#include "r2r_primary_contribution.h"
#include "r2r_path_contribution.h"
#include <thrust/set_operations.h>
#include <thrust/execution_policy.h>
#include <thrust/sequence.h>
#include <thrust/remove.h>
#include <pybind11/embed.h> 

namespace py = pybind11;

// We need a 6D light sampler for BPDT, actually the light_shape_id and light_tri_id is not that useful  
// A 6D light sampler.

template <typename T>
struct TLightSample6D {
	T light_sel;
	T tri_sel;
	TVector2<T> uv;
	TVector2<T> uv2;
};
using LightSample6D = TLightSample6D<Real>;

struct emitterSubPathBuffer {
	emitterSubPathBuffer(int max_bounces,
		int num_pixels,
		bool use_gpu) :
		num_pixels(num_pixels)
	{
		// For light tracing, we need to allocate memory for all bounces
		light_samples = Buffer<LightSample>(use_gpu, num_pixels);              // different from the path tracing, we only the primary light samples.
		light_samples_dir = Buffer<CameraSample>(use_gpu, num_pixels);              // different from the path tracing, 
		bsdf_samples = Buffer<BSDFSample>(use_gpu, max_bounces * num_pixels);  // for all bounces.
		light_rays = Buffer<Ray>(use_gpu, (max_bounces + 1) * num_pixels);     // for all bounces. ?  to check here. perhaps 

		primary_active_pixels = Buffer<int>(use_gpu, num_pixels);              // Primary pixels 
		active_pixels = Buffer<int>(use_gpu, (max_bounces + 1) * num_pixels);  // for all bounces.

		light_source_isects = Buffer<Intersection>(use_gpu, num_pixels);// Souces light isects 
		light_source_points = Buffer<SurfacePoint>(use_gpu, num_pixels);// Souces light points 

		// TODO : there are a memery bug here. we currently allocate more memory now. 
		//         + 1 num_pixels
		light_ray_isects = Buffer<Intersection>(use_gpu, (max_bounces + 1) * num_pixels);// notice that this is not the same concept with light_isects in PathBuffer structure, this is the actual intersecitons between the rays shoots from the light sources with the actual scenes.
		light_ray_points = Buffer<SurfacePoint>(use_gpu, (max_bounces + 1) * num_pixels);// the same. the actual intersections between light rays with the shapes during each bounces.

		min_roughness = Buffer<Real>(use_gpu, (max_bounces + 1) * num_pixels);
		throughputs = Buffer<Vector3>(use_gpu, (max_bounces + 1) * num_pixels);

		primary_ray_differentials = Buffer<RayDifferential>(use_gpu, num_pixels);
		ray_differentials = Buffer<RayDifferential>(use_gpu, (max_bounces + 1) * num_pixels);
		bsdf_ray_differentials = Buffer<RayDifferential>(use_gpu, max_bounces * num_pixels);

		invalid_isects = Buffer<Intersection>(use_gpu, num_pixels);
		DISPATCH(use_gpu, thrust::fill, invalid_isects.begin(), invalid_isects.end(), Intersection{ -1,-1 });
		//// OptiX buffers
		//optix_rays = Buffer<OptiXRay>(use_gpu, 2 * num_pixels);
		//optix_hits = Buffer<OptiXHit>(use_gpu, 2 * num_pixels);
	}
	int num_pixels;
	Buffer<LightSample> light_samples;  // we need light samples here, only related with the initial stage, in our implementation the numpixes. comment out - edge_light_samples; 
	Buffer<CameraSample> light_samples_dir;
	Buffer<BSDFSample> bsdf_samples;    // the same, we need bsdf_samples but from light direction, , edge_bsdf_samples;
	Buffer<Ray> light_rays;             // we need light rays. comment out -  nee_rays; 
										// comment out -  Buffer<Ray> edge_rays, edge_nee_rays;    Buffer<RayDifferential> primary_ray_differentials; Buffer<RayDifferential> ray_differentials, bsdf_ray_differentials;	Buffer<RayDifferential> edge_ray_differentials;
	Buffer<int> primary_active_pixels, active_pixels;// Sure, we need the same structure to accelate the light rays and elimination of inactivate rays when nesserary.

	Buffer<Intersection> light_source_isects;// 
	Buffer<SurfacePoint> light_source_points;// 

	Buffer<Intersection> light_ray_isects;// Sure, we need intersections materials to cacluate the bsdf_pdf for accumulation at each bounces. comment out - , edge_shading_isects;
	Buffer<SurfacePoint> light_ray_points;// Sure, we definetely need record intersections at each bounces to calculate the "connection" between stages ( s and t ) . comment out - , edge_shading_points;

	Buffer<Real> min_roughness;           // , edge_min_roughness;
	Buffer<Vector3> throughputs;          // , edge_throughputs;

	Buffer<Intersection> invalid_isects;//


	/*
		Below may important, may not.
	*/
	Buffer<RayDifferential> primary_ray_differentials;
	Buffer<RayDifferential> ray_differentials, bsdf_ray_differentials;
	//// OptiX related
	//Buffer<OptiXRay> optix_rays;
	//Buffer<OptiXHit> optix_hits;
};
//struct cameraPathEdgeBuffer 
//{};

struct cameraSubPathBuffer
{
	cameraSubPathBuffer(int max_bounces,
		int num_pixels,
		bool use_gpu) :
		num_pixels(num_pixels)
	{
		// For light tracing, we need to allocate memory for all bounces
		camera_samples = Buffer<CameraSample>(use_gpu, num_pixels);              // different from the path tracing, we only the primary light samples.
		bsdf_samples = Buffer<BSDFSample>(use_gpu, max_bounces * num_pixels);  // for all bounces.
		rays = Buffer<Ray>(use_gpu, (max_bounces + 1) * num_pixels);     // for all bounces. ?  to check here. perhaps 
		nee_rays = Buffer<Ray>(use_gpu, (max_bounces + 1) * num_pixels);     // for all bounces. ?  to check here. perhaps 

		primary_active_pixels = Buffer<int>(use_gpu, num_pixels);              // Primary pixels 
		active_pixels = Buffer<int>(use_gpu, (max_bounces + 1) * num_pixels);  // for all bounces.

		//light_source_isects = Buffer<Intersection>(use_gpu, num_pixels);// Souces light isects 
		//light_source_points = Buffer<SurfacePoint>(use_gpu, num_pixels);// Souces light points 

		// TODO : there are a memery bug here. we currently allocate more memory now. 
		//         + 1 num_pixels
		shading_isects = Buffer<Intersection>(use_gpu, (max_bounces + 1) * num_pixels);// notice that this is not the same concept with light_isects in PathBuffer structure, this is the actual intersecitons between the rays shoots from the light sources with the actual scenes.
		shading_points = Buffer<SurfacePoint>(use_gpu, (max_bounces + 1) * num_pixels);// the same. the actual intersections between light rays with the shapes during each bounces.

		min_roughness = Buffer<Real>(use_gpu, (max_bounces + 1) * num_pixels);
		throughputs = Buffer<Vector3>(use_gpu, (max_bounces + 1) * num_pixels);

		primary_ray_differentials = Buffer<RayDifferential>(use_gpu, num_pixels);
		ray_differentials = Buffer<RayDifferential>(use_gpu, (max_bounces + 1) * num_pixels);
		bsdf_ray_differentials = Buffer<RayDifferential>(use_gpu, max_bounces * num_pixels);

		//// OptiX buffers
		//optix_rays = Buffer<OptiXRay>(use_gpu, 2 * num_pixels);
		//optix_hits = Buffer<OptiXHit>(use_gpu, 2 * num_pixels);


				// Derivatives buffers
		d_next_throughputs = Buffer<Vector3>(use_gpu, num_pixels);
		d_next_rays = Buffer<DRay>(use_gpu, num_pixels);
		d_next_ray_differentials = Buffer<RayDifferential>(use_gpu, num_pixels);
		d_next_points = Buffer<SurfacePoint>(use_gpu, num_pixels);
		d_throughputs = Buffer<Vector3>(use_gpu, num_pixels);
		d_rays = Buffer<DRay>(use_gpu, num_pixels);
		d_ray_differentials = Buffer<RayDifferential>(use_gpu, num_pixels);
		d_points = Buffer<SurfacePoint>(use_gpu, num_pixels);

		d_general_vertices = Buffer<DVertex>(use_gpu, 3 * num_pixels);
		d_light_vertices = Buffer<DVertex>(use_gpu, 3 * num_pixels);
		d_bsdf_vertices = Buffer<DVertex>(use_gpu, 3 * num_pixels);
		d_diffuse_texs = Buffer<DTexture3>(use_gpu, num_pixels);
		d_specular_texs = Buffer<DTexture3>(use_gpu, num_pixels);
		d_roughness_texs = Buffer<DTexture1>(use_gpu, num_pixels);
		d_direct_lights = Buffer<DAreaLightInst>(use_gpu, num_pixels);
		d_nee_lights = Buffer<DAreaLightInst>(use_gpu, num_pixels);
		d_bsdf_lights = Buffer<DAreaLightInst>(use_gpu, num_pixels);
		d_envmap_vals = Buffer<DTexture3>(use_gpu, num_pixels);
		d_world_to_envs = Buffer<Matrix4x4>(use_gpu, num_pixels);

		d_vertex_reduce_buffer = Buffer<DVertex>(use_gpu, 3 * num_pixels);
		d_tex3_reduce_buffer = Buffer<DTexture3>(use_gpu, num_pixels);
		d_tex1_reduce_buffer = Buffer<DTexture1>(use_gpu, num_pixels);
		d_lgt_reduce_buffer = Buffer<DAreaLightInst>(use_gpu, num_pixels);
		d_envmap_reduce_buffer = Buffer<DTexture3>(use_gpu, num_pixels);

		d_cameras = Buffer<DCameraInst>(use_gpu, num_pixels);

		primary_edge_samples = Buffer<PrimaryEdgeSample>(use_gpu, num_pixels);
		secondary_edge_samples = Buffer<SecondaryEdgeSample>(use_gpu, num_pixels);
		primary_edge_records = Buffer<PrimaryEdgeRecord>(use_gpu, num_pixels);
		secondary_edge_records = Buffer<SecondaryEdgeRecord>(use_gpu, num_pixels);
		edge_contribs = Buffer<Real>(use_gpu, 2 * num_pixels);
		edge_surface_points = Buffer<Vector3>(use_gpu, 2 * num_pixels);

		tmp_light_samples = Buffer<LightSample>(use_gpu, num_pixels);
		tmp_bsdf_samples = Buffer<BSDFSample>(use_gpu, num_pixels);

	}
	int num_pixels;
	Buffer<CameraSample> camera_samples;  // we need light samples here, only related with the initial stage, in our implementation the numpixes. comment out - edge_light_samples; 
	Buffer<BSDFSample> bsdf_samples;    // the same, we need bsdf_samples but from light direction, , edge_bsdf_samples;
	Buffer<Ray> rays, nee_rays;             // we need light rays. 
										// comment out -  Buffer<Ray> edge_rays, edge_nee_rays;    Buffer<RayDifferential> primary_ray_differentials; Buffer<RayDifferential> ray_differentials, bsdf_ray_differentials;	Buffer<RayDifferential> edge_ray_differentials;
	Buffer<int> primary_active_pixels, active_pixels;// Sure, we need the same structure to accelate the light rays and elimination of inactivate rays when nesserary.

	//Buffer<Intersection> light_source_isects;// 
	//Buffer<SurfacePoint> light_source_points;// 


	Buffer<Intersection> shading_isects;// Sure, we need intersections materials to cacluate the bsdf_pdf for accumulation at each bounces. comment out - , edge_shading_isects;
	Buffer<SurfacePoint> shading_points;// Sure, we definetely need record intersections at each bounces to calculate the "connection" between stages ( s and t ) . comment out - , edge_shading_points;

	Buffer<Real> min_roughness;           // , edge_min_roughness;
	Buffer<Vector3> throughputs;          // , edge_throughputs;

	/*
		Below may important, may not.
	*/
	Buffer<RayDifferential> primary_ray_differentials;
	Buffer<RayDifferential> ray_differentials, bsdf_ray_differentials;
	//// OptiX related
	//Buffer<OptiXRay> optix_rays;
	//Buffer<OptiXHit> optix_hits;

		// Derivatives related
	Buffer<Vector3> d_next_throughputs;
	Buffer<DRay> d_next_rays;
	Buffer<RayDifferential> d_next_ray_differentials;
	Buffer<SurfacePoint> d_next_points;
	Buffer<Vector3> d_throughputs;
	Buffer<DRay> d_rays;
	Buffer<RayDifferential> d_ray_differentials;
	Buffer<SurfacePoint> d_points;

	Buffer<DVertex> d_general_vertices;
	Buffer<DVertex> d_light_vertices;
	Buffer<DVertex> d_bsdf_vertices;
	Buffer<DTexture3> d_diffuse_texs;
	Buffer<DTexture3> d_specular_texs;
	Buffer<DTexture1> d_roughness_texs;
	Buffer<DAreaLightInst> d_direct_lights;
	Buffer<DAreaLightInst> d_nee_lights;
	Buffer<DAreaLightInst> d_bsdf_lights;
	Buffer<DTexture3> d_envmap_vals;
	Buffer<Matrix4x4> d_world_to_envs;
	Buffer<DVertex> d_vertex_reduce_buffer;
	Buffer<DTexture3> d_tex3_reduce_buffer;
	Buffer<DTexture1> d_tex1_reduce_buffer;
	Buffer<DAreaLightInst> d_lgt_reduce_buffer;
	Buffer<DTexture3> d_envmap_reduce_buffer;

	Buffer<DCameraInst> d_cameras;

	// Edge sampling related
	Buffer<PrimaryEdgeSample> primary_edge_samples;
	Buffer<SecondaryEdgeSample> secondary_edge_samples;
	Buffer<PrimaryEdgeRecord> primary_edge_records;
	Buffer<SecondaryEdgeRecord> secondary_edge_records;
	Buffer<Real> edge_contribs;
	Buffer<Vector3> edge_surface_points;
	// For sharing RNG between pixels
	Buffer<LightSample> tmp_light_samples;
	Buffer<BSDFSample> tmp_bsdf_samples;


};

//auto local_dir = cos_hemisphere(bsdf_sample.uv);

struct light_ray_sampler {
	DEVICE void operator()(int idx)
	{
		auto pixel_id = active_pixels[idx];

		// Select light source by binary search on light_cdf
		auto sample = samples[pixel_id];
		auto light_dir_sample = light_dir_samples[pixel_id];
		const Real *light_ptr = thrust::upper_bound(thrust::seq, scene.light_cdf, scene.light_cdf + scene.num_lights, sample.light_sel);
		auto light_id = clamp((int)(light_ptr - scene.light_cdf - 1), 0, scene.num_lights - 1);


		// HACK: here, we did not implement a 6d light sampler yet.
		// Since the tri_sel and light_sel are usually selected in a very small selection, for example, one square light (two triangles). 			
		Vector2 dir_sample(light_dir_sample.xy);

		auto local_dir = normalize(cos_hemisphere(dir_sample));

		if (scene.envmap != nullptr && light_id == scene.num_lights - 1) {
			// Environment map
			light_source_isects[pixel_id].shape_id = -1;
			light_source_isects[pixel_id].tri_id = -1;
			light_source_points[pixel_id] = SurfacePoint::zero();

			// Swap the dir and org
			light_rays[pixel_id].org = envmap_sample(*(scene.envmap), sample.uv);
			light_rays[pixel_id].dir = to_world
			(light_source_points[pixel_id].shading_frame, local_dir);
			light_rays[pixel_id].tmin = 1e-3f;
			light_rays[pixel_id].tmax = infinity<Real>();
		}
		else
		{
			// Area light
			const auto &light = scene.area_lights[light_id];
			const auto &shape = scene.shapes[light.shape_id];
			// Select triangle by binary search on area_cdfs
			const Real *area_cdf = scene.area_cdfs[light_id];
			const Real *tri_ptr = thrust::upper_bound(thrust::seq, area_cdf, area_cdf + shape.num_triangles, sample.tri_sel);
			// Point Light  Hack 
			//const Real *tri_ptr = thrust::upper_bound(thrust::seq,area_cdf, area_cdf + shape.num_triangles, 0.5);

			auto tri_id = clamp((int)(tri_ptr - area_cdf - 1), 0, shape.num_triangles - 1);

			light_source_isects[pixel_id].shape_id = light.shape_id;                // Record the light source shape id.
			light_source_isects[pixel_id].tri_id = tri_id;                          // Record the light source triangle id.

			light_source_points[pixel_id] = sample_shape(shape, tri_id, sample.uv); // Get sampled location from the light source triangle.

			// Point Light  Hack 
			//light_source_points[pixel_id] = sample_shape(shape, tri_id, Vector2(0.333,0.618)); // Get sampled location from the light source triangle.		

			// swap the dir and org
			light_rays[pixel_id].org = light_source_points[pixel_id].position;      //get the start point
			light_rays[pixel_id].dir = 
			to_world(light_source_points[pixel_id].shading_frame, local_dir);

			// Shadow epislon. Sorry.
			light_rays[pixel_id].tmin = 1e-3f;
			light_rays[pixel_id].tmax = infinity<Real>(); // infinity to hit everything.
		}
	}

	const FlattenScene scene;
	const int *active_pixels;
	const LightSample *samples;
	const CameraSample * light_dir_samples;
	Intersection *light_source_isects;
	SurfacePoint *light_source_points;
	Ray *light_rays;
};

/*
	Random point on the light emitters
	Random direction {random from shape list, random from triangle list}
*/
void create_rays_from_emitter(const Scene &scene,
	const BufferView<int> &active_pixels,
	const BufferView<LightSample> &samples,
	const BufferView<CameraSample> &light_dir_samples,
	BufferView<Intersection> light_source_isects,
	BufferView<SurfacePoint> light_source_points,
	BufferView<Ray> light_rays) {
	parallel_for(light_ray_sampler{
		get_flatten_scene(scene),
		active_pixels.begin(),
		samples.begin(),
		light_dir_samples.begin(),
		light_source_isects.begin(),
		light_source_points.begin(),
		light_rays.begin() },
		active_pixels.size(), scene.use_gpu);
}
struct create_connect_ray {
	DEVICE void operator()(int idx)
	{
		auto pixel_id = active_pixels[idx];
		shadow_rays[pixel_id].org = shading_points[pixel_id].position;
		shadow_rays[pixel_id].dir = normalize(
			light_points[pixel_id].position - shading_points[pixel_id].position);
		// Shadow epislon. Sorry.
		shadow_rays[pixel_id].tmin = 1e-3f;
		shadow_rays[pixel_id].tmax = (1 - 1e-3f) *
			length(light_points[pixel_id].position - shading_points[pixel_id].position);
	}
	const int *active_pixels;
	const SurfacePoint *shading_points;
	const SurfacePoint *light_points;
	Ray *shadow_rays;
};

void build_connection_rays(const Scene &scene,
	const BufferView<int> &common_active_pixels,
	const BufferView<SurfacePoint> shading_points,
	const BufferView<SurfacePoint> light_source_points,
	BufferView<Ray> shadow_rays) {
	parallel_for(create_connect_ray{
		common_active_pixels.begin(),
		shading_points.begin(),
		light_source_points.begin(),
		shadow_rays.begin() },
		common_active_pixels.size(), scene.use_gpu);
}

void init_light_active_pixels(BufferView<int> &active_pixels, bool use_gpu, ThrustCachedAllocator &thrust_alloc) {
	DISPATCH(use_gpu, thrust::sequence, active_pixels.begin(), active_pixels.end());
	active_pixels.count = active_pixels.end() - active_pixels.begin();
}

void reset_active_pixels(BufferView<int> &active_pixels, int num_pixels, bool use_gpu, ThrustCachedAllocator &thrust_alloc) {
	DISPATCH(use_gpu, thrust::sequence, active_pixels.begin(), active_pixels.end());
	active_pixels.count = active_pixels.end() - active_pixels.begin();
}


void intersect_active_pixels(const BufferView<int> &active_pixels_set1, const BufferView<int> &active_pixels_set2,
	BufferView<int> &comm_active_pixels,
	bool use_gpu) {
	auto new_end = DISPATCH(use_gpu, thrust::set_intersection,
		active_pixels_set1.begin(), active_pixels_set1.end(),
		active_pixels_set2.begin(), active_pixels_set2.end(),
		comm_active_pixels.begin());
	comm_active_pixels.count = new_end - comm_active_pixels.begin();
}

struct cal_light_primary_thoughout {
	DEVICE void operator()(int idx) {
		auto pixel_id = active_pixels[idx];
		const auto light_ray = light_rays[pixel_id];
		const auto light_source_isect = light_source_isects[pixel_id];
		const auto light_source_point = light_source_points[pixel_id];
		const auto light_hit_isect = light_hit_isects[pixel_id];
		const auto light_hit_point = light_hit_points[pixel_id];
		auto &light_thoughtput = light_throughputs[pixel_id];
		auto Le = Vector3{ 0, 0, 0 };
		const auto &light_shape = scene.shapes[light_source_isect.shape_id];
		const auto &light = scene.area_lights[light_shape.light_id];

		auto light_ray_dir = light_hit_point.position - light_source_point.position;		   // reverse direction  -->  auto dir = light_point.position - p;
		auto dist_sq = length_squared(light_ray_dir);
		auto light_ray_wo = light_ray_dir / sqrt(dist_sq);
		
		float cosineL = fabs(dot(normalize(light_ray_wo), light_source_point.geom_normal));

		//if (light.two_sided || dot(normalize(light_ray_wo), light_source_point.shading_frame.n) > 0 ) 
		{
			auto geometry_term = fabs(dot(normalize(light_ray_wo), light_source_point.geom_normal)) / dist_sq;
			auto light_pmf = scene.light_pmf[light_shape.light_id];
			auto light_area = scene.light_areas[light_shape.light_id];
			//auto pdf_light = cosineL * light_pmf / (light_area * float(M_PI));
			auto pdf_light = light_pmf / (light_area );
			auto pdf_bsdf = 1.0;//bsdf_pdf(material, light_hit_point, wi, wo, min_rough) * geometry_term;
			auto mis_weight = square(pdf_light) / (square(pdf_light) + square(pdf_bsdf));
			Le =  ( 1.0 * float(M_PI)  /  (  pdf_light * cosineL )) * light.intensity;  // * cosineL * mis_weight
			//( 200.0 /  pdf_nee) * light.intensity; // delete  geometry_term  
			//fabs(dot(normalize(light_ray_wo), normalize(light_source_point.geom_normal))) 
		}
		light_thoughtput = Le;
		//light_thoughtput = Vector3(1.0, 1.0,1.0) * light.intensity;
	}
	const FlattenScene scene;
	const int *active_pixels;
	const Ray *light_rays;
	const Intersection *light_source_isects;
	const SurfacePoint *light_source_points;
	const Intersection *light_hit_isects;
	const SurfacePoint *light_hit_points;
	Vector3 *light_throughputs;
};

// We calcualted the intensity geometry term here.

void light_primary_thoughout(const Scene &scene,
	const BufferView<int> &active_pixels,
	const BufferView<Ray> &light_rays,
	const BufferView<Intersection> &light_source_isects,
	const BufferView<SurfacePoint> &light_source_points,
	const BufferView<Intersection> &light_hit_isects,
	const BufferView<SurfacePoint> &light_hit_points,
	BufferView<Vector3> &light_throughputs
	//const Real weight, // 1.0 / lightsamples 	
) {
	parallel_for(cal_light_primary_thoughout{
		get_flatten_scene(scene),
		active_pixels.begin(),
		light_rays.begin(),
		light_source_isects.begin(),
		light_source_points.begin(),
		light_hit_isects.begin(),
		light_hit_points.begin(),
		light_throughputs.begin()
		}, active_pixels.size(), scene.use_gpu);
}

struct light_path_contribs_accumulator {
	DEVICE void operator()(int idx) {
		auto pixel_id = active_pixels[idx];
		const auto &throughput = light_throughputs[pixel_id];
		const auto &incoming_ray = light_incoming_rays[pixel_id];
		const auto &light_hit_isect = light_hit_isects[pixel_id];
		const auto &light_hit_point = light_hit_points[pixel_id];
		const auto &bsdf_isect = bsdf_isects[pixel_id];
		const auto &bsdf_point = bsdf_points[pixel_id];
		const auto &bsdf_ray = bsdf_rays[pixel_id];
		const auto &min_rough = min_roughness[pixel_id];
		auto &next_throughput = next_throughputs[pixel_id];

		/*
			*/
		auto wi = -incoming_ray.dir;
		auto p = light_hit_point.position;
		const auto &ligth_hit_shape = scene.shapes[light_hit_isect.shape_id];
		const auto &material = scene.materials[ligth_hit_shape.material_id];

		auto scatter_bsdf = Vector3{ 0, 1000, 0 };
		next_throughput = Vector3{ 0, 0, 0 };
		if (light_hit_isect.valid())
		{
			const auto &ligth_hit_shape = scene.shapes[light_hit_isect.shape_id];
			auto dir = bsdf_point.position - p;
			auto dist_sq = length_squared(dir);
			auto wo = dir / sqrt(dist_sq);
			auto pdf_bsdf = bsdf_pdf(material, light_hit_point, wi, wo, min_rough);
			if (pdf_bsdf > 1e-20f) {
				auto bsdf_val = bsdf(material, light_hit_point, wi, wo, min_rough);
				// do not handle the case : light ray hit another light source.
				scatter_bsdf = bsdf_val / pdf_bsdf;
				next_throughput = throughput * bsdf_val;
			}
			else {
				next_throughput = Vector3{ 0, 0, 0 };
			}
		}
	}

	const FlattenScene         scene;
	const int                  *active_pixels;
	const Vector3              *light_throughputs;
	const Ray                  *light_incoming_rays;
	const Intersection         *light_hit_isects;
	const SurfacePoint         *light_hit_points;
	const Intersection         *bsdf_isects;
	const SurfacePoint         *bsdf_points;
	const Ray                  *bsdf_rays;
	const Real                 *min_roughness;
	const Real                 weight;
	const ChannelInfo          channel_info;
	Vector3                    *next_throughputs;
};

void accumulate_light_path_contribs(const Scene &scene,
	const BufferView<int>                       &active_pixels,
	const BufferView<Vector3>                   &light_throughputs,
	const BufferView<Ray>                       &light_incoming_rays,
	const BufferView<Intersection>              &light_hit_isects,
	const BufferView<SurfacePoint>              &light_hit_points,
	const BufferView<Intersection>              &bsdf_isects,
	const BufferView<SurfacePoint>              &bsdf_points,
	const BufferView<Ray>                       &bsdf_rays,
	const BufferView<Real>                      &min_roughness,
	const Real                                  weight,
	const ChannelInfo                           &channel_info,
	BufferView<Vector3>                         next_throughputs
/*BufferView<Real> edge_contribs*/) {
	parallel_for(light_path_contribs_accumulator{
		get_flatten_scene(scene),
		active_pixels.begin(),
		light_throughputs.begin(),
		light_incoming_rays.begin(),
		light_hit_isects.begin(),
		light_hit_points.begin(),
		bsdf_isects.begin(),
		bsdf_points.begin(),
		bsdf_rays.begin(),
		min_roughness.begin(),
		weight,
		channel_info,
		next_throughputs.begin()
		/*rendered_image,
		edge_contribs.begin()*/ }, active_pixels.size(), scene.use_gpu);
}




struct bdpt_evalPath
{
	DEVICE void operator()(int idx) {
		auto pixel_id = active_pixels[idx];
		//const FlattenScene scene;
		//const int *active_pixels;
		const auto &incomingC = incoming_rays_C[pixel_id];
		const auto &shadow_ray = shadow_rays[pixel_id];
		const auto &incomingL = incoming_rays_L[pixel_id];
		const auto &shading_isect = shading_isects[pixel_id];
		const auto &shading_point = shading_points[pixel_id];
		const auto &min_rough_C = min_roughness_C[pixel_id];
		const auto &min_rough_L = min_roughness_L[pixel_id];
		const auto &light_isect = L_isects[pixel_id];
		const auto &light_point = L_points[pixel_id];
		const auto &throughputC = camera_path_throughputs[pixel_id];
		const auto &throughputL = light_path_throughputs[pixel_id];

		auto light_wo = -shadow_ray.dir; // -ray_wo		
		const auto &light_pt_shading_shape = scene.shapes[light_isect.shape_id];

		const auto &shading_shape = scene.shapes[shading_isect.shape_id];
		const auto &materialC = scene.materials[shading_shape.material_id];
		const auto &materialL = scene.materials[light_pt_shading_shape.material_id];

		Vector3 bsdfL = Vector3(0.f, 0.f, 0.f);
		Vector3 bsdfC = Vector3(0.f, 0.f, 0.f);

		float cosineL = 0.0;
		float cosineC = 0.0;

		float shadowDistance2 = 0.0;
		float geometryFactor = 0.0;

		auto joint_contrib = Vector3(0.f, 0.f, 0.f);

		if (shadow_ray.tmax >= 0) { // tmax < 0 means the ray is blocked
			if (light_isect.valid()) {
				// shape from light pt
				auto dir = light_point.position - shading_point.position;
				auto dist_sq = length_squared(dir);
				shadowDistance2 = dist_sq;//shadow_ray.tmax * shadow_ray.tmax;
				if (shadowDistance2 > 1e-20)
				{
					SurfacePoint nl_pt = light_point;
					nl_pt.geom_normal = normalize(nl_pt.geom_normal);
					nl_pt.shading_frame.n = normalize(nl_pt.shading_frame.n);
					auto normalL = normalize(light_point.geom_normal);
					auto normalC = normalize(shading_point.geom_normal);
					//auto normalL = normalize(light_point.shading_frame.n);
					//auto normalC = normalize(shading_point.shading_frame.n);
					
					cosineC = dot(normalize(-shadow_ray.dir), normalC);
					cosineL = dot(normalize(shadow_ray.dir), normalL);
					geometryFactor = fabs(cosineL * cosineC) / shadowDistance2;
					// C

					if (cosineC > 0) {
						auto bsdfCVal = bsdf(materialC, shading_point, -incomingC.dir, shadow_ray.dir, min_rough_C);
						auto pdf_bsdf_C = bsdf_pdf(materialC, shading_point, -incomingC.dir, shadow_ray.dir, min_rough_C);
						bsdfC = bsdfCVal;// / pdf_bsdf_C;
					}
					// L				
					if (cosineL > 0) {
						auto bsdfLVal = bsdf(materialL, nl_pt, -incomingL.dir, -shadow_ray.dir, min_rough_L);
						auto pdf_bsdf_L = bsdf_pdf(materialL, nl_pt, -incomingL.dir, -shadow_ray.dir, min_rough_L);
						bsdfL = bsdfLVal;// / pdf_bsdf_L;                      
					}
					joint_contrib = (throughputC * bsdfC * geometryFactor *  bsdfL * throughputL);
					// scene.light_cdf +
					//joint_contrib = (throughputC * 10000);
				}
			}
		}		
		assert(isfinite(joint_contrib));		
		if (rendered_image != nullptr) {
			auto nd = channel_info.num_total_dimensions;
			auto d = channel_info.radiance_dimension;
			rendered_image[nd * pixel_id + d] += weight * joint_contrib[0];
			rendered_image[nd * pixel_id + d + 1] += weight * joint_contrib[1];
			rendered_image[nd * pixel_id + d + 2] += weight * joint_contrib[2];
		}
		//if (edge_contribs != nullptr) {
		//	edge_contribs[pixel_id] += sum(weight * path_contrib);
		//}
	}
	const FlattenScene scene;
	const int *active_pixels;
	const Ray * incoming_rays_C;
	const Ray * shadow_rays;
	const Ray * incoming_rays_L;
	const Intersection * shading_isects;
	const SurfacePoint * shading_points;
	const Real *min_roughness_C;
	const Real *min_roughness_L;
	const Intersection * L_isects;
	const SurfacePoint * L_points;
	const Vector3 * camera_path_throughputs;
	const Vector3 * light_path_throughputs;
	const Real weight;
	const ChannelInfo channel_info;
	float * rendered_image;
};

void eval_path_conection(
	const Scene &scene,
	const BufferView<int> &common_active_pixels,
	const BufferView<Ray> &incoming_rays_C,
	const BufferView<Ray> &shadow_rays,
	const BufferView<Ray> &incoming_rays_L,
	const BufferView<Intersection> &C_isects,
	const BufferView<SurfacePoint> &C_points,
	const BufferView<Real> &min_roughness_C,
	const BufferView<Real> &min_roughness_L,
	const BufferView<Intersection> &L_isects,
	const BufferView<SurfacePoint> &L_points,
	const BufferView<Vector3> &camera_path_throughputs,
	const BufferView<Vector3> &light_path_throughputs,
	const Real weight,
	const ChannelInfo &channel_info,
	float * rendered_image) {
	parallel_for(bdpt_evalPath{
		get_flatten_scene(scene),
		common_active_pixels.begin(),
		incoming_rays_C.begin(),
		shadow_rays.begin(),
		incoming_rays_L.begin(),
		C_isects.begin(),
		C_points.begin(),
		min_roughness_C.begin(),
		min_roughness_L.begin(),
		L_isects.begin(),
		L_points.begin(),
		camera_path_throughputs.begin(),
		light_path_throughputs.begin(),
		weight,
		channel_info,
		rendered_image
		}, common_active_pixels.size(), scene.use_gpu);
}


// Firstly, let us finish the forward rendering.
void r2r_bdpt_render(const Scene &scene,
	const RenderOptions &options,
	ptr<float> rendered_image,
	ptr<float> d_rendered_image,
	std::shared_ptr<DScene> d_scene,
	ptr<float> debug_image,
	ptr<float> input_density_image) {
#ifdef __NVCC__
	int old_device_id = -1;
	if (scene.use_gpu) {
		checkCuda(cudaGetDevice(&old_device_id));
		if (scene.gpu_index != -1) {
			checkCuda(cudaSetDevice(scene.gpu_index));
		}
	}
#endif
	parallel_init();
	py::print("r2r bdpt render function begins...\n");
	//BufferView<int> a(0, 4);
	//a.data[0] = 1;  a.data[1] = 2; a.data[2] = 10;	a.data[3] = 11;
	//BufferView<int> b(0, 3);
	//b.data[0] = 2; b.data[1] = 10; b.data[2] = 12;
	//BufferView<int> c(0, 5);
	//intersect_active_pixels(a, b, c,scene.use_gpu);
	//py::print(c.data[0], c.data[1], c.data[2],c.data[3], c.data[4]);

	ChannelInfo channel_info(options.channels, scene.use_gpu);
	const auto &camera = scene.camera;
	auto num_pixels = camera.width * camera.height;
	auto max_bounces = options.max_bounces;
	auto light_ray_max_bounces = max_bounces;

	// The emitter(light sources) sub paths buffer.
	emitterSubPathBuffer emitter_sub_path_buffer(max_bounces, num_pixels, scene.use_gpu);
	// The camera sub paths buffer.
	cameraSubPathBuffer camera_sub_path_buffer(max_bounces, num_pixels, scene.use_gpu);
	// The large path buffer for everything.	
	// For PT max_bounces
	// For BDPT max_bounces * max_lightray_bounces + 1 
	PathBuffer path_buffer((max_bounces * light_ray_max_bounces + 1), num_pixels, scene.use_gpu, channel_info);

	auto light_num_active_pixels = std::vector<int>((light_ray_max_bounces + 1) * num_pixels, 0);  // Light pixel id control -> perhaps not need to equal to num_pixels
	auto camera_num_active_pixels = std::vector<int>((max_bounces + 1) * num_pixels, 0);
	auto num_active_pixels = std::vector<int>((max_bounces + 1) * num_pixels, 0);         // The actually pixel id control for final accumulate.
	auto common_active_pixels = std::vector<int>((max_bounces + 1) * num_pixels, 0);         // The actually pixel id control for final accumulate.

	std::unique_ptr<Sampler> sampler, emitter_sampler, edge_sampler;
	sampler = std::unique_ptr<Sampler>(new PCGSampler(scene.use_gpu, options.seed, num_pixels));                // 使用 random sampler
	emitter_sampler = std::unique_ptr<Sampler>(new PCGSampler(scene.use_gpu, options.seed, num_pixels));        // 使用 random sampler	
	edge_sampler = std::unique_ptr<Sampler>(new PCGSampler(scene.use_gpu, options.seed + 131071U, num_pixels)); // 使用 random sampler

	auto optix_rays = path_buffer.optix_rays.view(0, 2 * num_pixels);
	auto optix_hits = path_buffer.optix_hits.view(0, 2 * num_pixels);
	ThrustCachedAllocator thrust_alloc(scene.use_gpu, num_pixels * sizeof(DTexture3));

	const auto invalid_isects = emitter_sub_path_buffer.invalid_isects.view(0, num_pixels);
	/*  For each sample, random  walk from the screen and light path in the same time. Then merge suitble sub paths
	Then we fuck the whole derevetive calcualtion pipeline. Pretty simple, right? */
	for (int sample_id = 0; sample_id < options.num_samples; sample_id++)
	{
		emitter_sampler->begin_sample(sample_id);
		sampler->begin_sample(sample_id);
        
		/* Part 1 : record all the paths from emitter(light sources). */
		/* Part 1.1 : initialization of all the states for light primary intersection states.*/
		auto light_samples = emitter_sub_path_buffer.light_samples.view(0, num_pixels);
		auto light_samples_dir = emitter_sub_path_buffer.light_samples_dir.view(0, num_pixels);
		auto light_primary_rays = emitter_sub_path_buffer.light_rays.view(0, num_pixels);

		auto light_source_isects = emitter_sub_path_buffer.light_source_isects.view(0, num_pixels);
		auto light_source_points = emitter_sub_path_buffer.light_source_points.view(0, num_pixels);

		auto light_primary_isects = emitter_sub_path_buffer.light_ray_isects.view(0, num_pixels);
		auto light_primary_points = emitter_sub_path_buffer.light_ray_points.view(0, num_pixels);

		auto light_primary_active_pixels = emitter_sub_path_buffer.primary_active_pixels.view(0, num_pixels);
		auto light_active_pixels = emitter_sub_path_buffer.active_pixels.view(0, num_pixels);
		auto light_min_roughness = emitter_sub_path_buffer.min_roughness.view(0, num_pixels);
		auto light_throughputs = emitter_sub_path_buffer.throughputs.view(0, num_pixels);

		// I leave these two here only for parameter passing. For faster computer, we need to refract code 
		auto light_primary_differentials = emitter_sub_path_buffer.primary_ray_differentials.view(0, num_pixels); // Actually, this is useless for now. We keep it only for quick and dirty implementation.
		auto light_ray_differentials = emitter_sub_path_buffer.ray_differentials.view(0, num_pixels);         // Actually, this is useless for now. Reuse the intersection.

		init_paths(light_throughputs, light_min_roughness, scene.use_gpu);
		// Sample points on lights， 6D samples needed here. 
        // Try to use the sample sampler
		sampler->next_light_samples(light_samples);
		sampler->next_camera_samples(light_samples_dir);
		// // make all 'light pixels' active.	

		init_light_active_pixels(light_primary_active_pixels, scene.use_gpu, thrust_alloc);
		// auto light_num_actives_primary = (int)light_primary_active_pixels.size();
		// // Create light rays from the sample.
		create_rays_from_emitter(scene, light_primary_active_pixels, light_samples, light_samples_dir, light_source_isects,
			light_source_points, light_primary_rays);

		// // Intersection with the scene.
		// // actually, we can just call optix / embree intersection here.
		intersect(scene,
			light_primary_active_pixels,
			light_primary_rays,
			light_primary_differentials,
			light_primary_isects,
			light_primary_points,
			light_ray_differentials,
			optix_rays,
			optix_hits);


		// Path throughput before L1. 
		light_primary_thoughout(scene,
			light_primary_active_pixels,
			light_primary_rays,
			light_source_isects,
			light_source_points,
			light_primary_isects,
			light_primary_points,
			light_throughputs);

		update_active_pixels(light_primary_active_pixels, light_primary_isects, light_active_pixels, scene.use_gpu);
		///py::print("\nlight primary pixels size:", light_primary_active_pixels.size());
		///py::print("light active pixels after primary intersection", light_active_pixels.size());
		// /* Part 1.2 : Now go deeper recursively.	*/
		std::fill(light_num_active_pixels.begin(), light_num_active_pixels.end(), 0);
		light_num_active_pixels[0] = light_active_pixels.size();

		for (int depth = 0; depth < light_ray_max_bounces && light_num_active_pixels[depth] > 0; depth++)
		{
			//// The alive light ray ids from light source. 
			const auto active_pixels = emitter_sub_path_buffer.active_pixels.view(depth * num_pixels, light_num_active_pixels[depth]);
			const auto light_ray_isects = emitter_sub_path_buffer.light_ray_isects.view(depth * num_pixels, num_pixels);
			const auto light_ray_points = emitter_sub_path_buffer.light_ray_points.view(depth * num_pixels, num_pixels);
			auto incoming_light_rays = emitter_sub_path_buffer.light_rays.view(depth * num_pixels, num_pixels);
			auto bsdf_samples = emitter_sub_path_buffer.bsdf_samples.view(depth * num_pixels, num_pixels);
			auto min_roughness = emitter_sub_path_buffer.min_roughness.view(depth * num_pixels, num_pixels);

			// I leave these three here only for parameter passing. For faster computation, we need to refract code 
			auto incoming_ray_differentials = emitter_sub_path_buffer.ray_differentials.view(depth * num_pixels, num_pixels); // TODO:
			auto bsdf_ray_differentials = emitter_sub_path_buffer.bsdf_ray_differentials.view(depth * num_pixels, num_pixels); // TODO:
			auto next_ray_differentials = emitter_sub_path_buffer.ray_differentials.view((depth + 1) * num_pixels, num_pixels); // TODO
			// no incoming_ray_differentials 

			auto light_throughputs = emitter_sub_path_buffer.throughputs.view(depth * num_pixels, num_pixels);

			// depth + 1 ...
			auto light_next_throughputs = emitter_sub_path_buffer.throughputs.view((depth + 1) * num_pixels, num_pixels);
			auto next_active_pixels = emitter_sub_path_buffer.active_pixels.view((depth + 1) * num_pixels, num_pixels);
			auto bsdf_isects = emitter_sub_path_buffer.light_ray_isects.view((depth + 1) * num_pixels, num_pixels);
			auto bsdf_points = emitter_sub_path_buffer.light_ray_points.view((depth + 1) * num_pixels, num_pixels);
			auto next_rays = emitter_sub_path_buffer.light_rays.view((depth + 1) * num_pixels, num_pixels);
			auto next_min_roughness = emitter_sub_path_buffer.min_roughness.view((depth + 1) * num_pixels, num_pixels);

			///// Sample directions based on BRDF
			sampler->next_bsdf_samples(bsdf_samples);
			bsdf_sample(scene,
				active_pixels,
				incoming_light_rays,
				incoming_ray_differentials, // Only for parameter passing . 
				light_ray_isects,
				light_ray_points,
				bsdf_samples,
				min_roughness,
				next_rays,
				bsdf_ray_differentials,    // Only for parameter passing . 
				next_min_roughness);

			// 	/// Intersect with the scene
			intersect(scene,
				active_pixels,
				next_rays,
				bsdf_ray_differentials,    // Only for parameter passing . 
				bsdf_isects,
				bsdf_points,
				next_ray_differentials, // Only for parameter passing . 
				optix_rays,
				optix_hits);
			// 	/// Stream compaction: remove invalid bsdf intersections. active_pixels -> next_active_pixels			
			//Accumulate 

			accumulate_light_path_contribs(scene,
				active_pixels,
				light_throughputs,
				incoming_light_rays,
				light_ray_isects,
				light_ray_points,
				bsdf_isects,
				bsdf_points,
				next_rays,
				min_roughness,
				Real(1.0),    // Not used here.
				channel_info, // Not used here.
				light_next_throughputs);


			update_active_pixels(active_pixels, bsdf_isects, next_active_pixels, scene.use_gpu);
			//py::print("light path at :", depth);
			//py::print("light active pixels size:", active_pixels.size());
			//py::print("light active pixels after intersection", next_active_pixels.size());

			// 	/// Record the number of active pixels for next depth			
			light_num_active_pixels[depth + 1] = next_active_pixels.size();
		}

		/* Now we finish the light tracing with bounces =, please remember wo do not actually record ray_differentials .  */
		/*
		   For now : Since we have re-calculate all the differentials, we have to re-trace the whole path, we probably only need to
		   Record the path vertex with (shape_id,tri_id)
		   All other intermedia things need to be recaculated!
		*/
		/*
			Part 2 : Shoot rays from the screen camera.
		*/
		auto throughputs = camera_sub_path_buffer.throughputs.view(0, num_pixels);
		auto camera_samples = camera_sub_path_buffer.camera_samples.view(0, num_pixels);
		auto rays = camera_sub_path_buffer.rays.view(0, num_pixels);
		auto shading_isects = camera_sub_path_buffer.shading_isects.view(0, num_pixels);
		auto shading_points = camera_sub_path_buffer.shading_points.view(0, num_pixels);
		auto min_roughness = camera_sub_path_buffer.min_roughness.view(0, num_pixels);
		auto primary_active_pixels = camera_sub_path_buffer.primary_active_pixels.view(0, num_pixels);
		auto active_pixels = camera_sub_path_buffer.active_pixels.view(0, num_pixels);
		auto primary_differentials = camera_sub_path_buffer.primary_ray_differentials.view(0, num_pixels);
		auto ray_differentials = camera_sub_path_buffer.ray_differentials.view(0, num_pixels);

		init_paths(throughputs, min_roughness, scene.use_gpu);
		// Generate primary ray samples
		sampler->next_camera_samples(camera_samples);
		sample_primary_rays(camera, camera_samples, rays, primary_differentials, scene.use_gpu);

		// Initialize pixel id
		init_active_pixels(rays, primary_active_pixels, scene.use_gpu, thrust_alloc);
		auto num_actives_primary = (int)primary_active_pixels.size();

		// Primary Intersect with the scene
		intersect(scene, primary_active_pixels, rays, primary_differentials, shading_isects, shading_points, ray_differentials, optix_rays, optix_hits);

		// Perhaps we can keep the primary accumulation here.
		accumulate_primary_contribs(scene, primary_active_pixels, throughputs, BufferView<Real>(), // channel multipliers
			rays,
			ray_differentials,
			shading_isects,
			shading_points,
			Real(1) / options.num_samples,
			channel_info,
			rendered_image.get(),
			BufferView<Real>()); // edge_contrib

		// Stream compaction: remove invalid intersection
		update_active_pixels(primary_active_pixels, shading_isects, active_pixels, scene.use_gpu);

		std::fill(camera_num_active_pixels.begin(), camera_num_active_pixels.end(), 0);
		camera_num_active_pixels[0] = active_pixels.size();
		for (int depth = 0; depth < max_bounces && camera_num_active_pixels[depth] > 0; depth++)
		{
			// Buffer views for this path vertex
			const auto active_pixels = camera_sub_path_buffer.active_pixels.view(depth * num_pixels, camera_num_active_pixels[depth]);
			const auto shading_isects = camera_sub_path_buffer.shading_isects.view(depth * num_pixels, num_pixels);
			const auto shading_points = camera_sub_path_buffer.shading_points.view(depth * num_pixels, num_pixels);
			auto bsdf_samples = camera_sub_path_buffer.bsdf_samples.view(depth * num_pixels, num_pixels);
			auto incoming_rays = camera_sub_path_buffer.rays.view(depth * num_pixels, num_pixels);
			auto min_roughness = camera_sub_path_buffer.min_roughness.view(depth * num_pixels, num_pixels);
			const auto throughputs = camera_sub_path_buffer.throughputs.view(depth * num_pixels, num_pixels);

			// I leave these three here only for parameter passing. For faster computation, we need to refract code 			
			auto incoming_ray_differentials = camera_sub_path_buffer.ray_differentials.view(depth * num_pixels, num_pixels);
			auto next_ray_differentials = camera_sub_path_buffer.ray_differentials.view((depth + 1) * num_pixels, num_pixels);
			auto bsdf_ray_differentials = camera_sub_path_buffer.bsdf_ray_differentials.view(depth * num_pixels, num_pixels);

			// Depth +1
			auto next_active_pixels = camera_sub_path_buffer.active_pixels.view((depth + 1) * num_pixels, num_pixels);
			auto next_rays = camera_sub_path_buffer.rays.view((depth + 1) * num_pixels, num_pixels);
			auto bsdf_isects = camera_sub_path_buffer.shading_isects.view((depth + 1) * num_pixels, num_pixels);
			auto bsdf_points = camera_sub_path_buffer.shading_points.view((depth + 1) * num_pixels, num_pixels);
			auto next_throughputs = camera_sub_path_buffer.throughputs.view((depth + 1) * num_pixels, num_pixels);
			auto next_min_roughness = camera_sub_path_buffer.min_roughness.view((depth + 1) * num_pixels, num_pixels);

			/// Decommet the shadow rays related code. 
			auto L0_nee_rays = camera_sub_path_buffer.nee_rays.view(depth * num_pixels, num_pixels);
			//auto light_isects = path_buffer.light_isects.view(depth * num_pixels, num_pixels);
			//auto light_points = path_buffer.light_points.view(depth * num_pixels, num_pixels);
			// Sample directions based on BRDF
			/*
				test here:
			*/
			auto light_source_isects = emitter_sub_path_buffer.light_source_isects.view(0, num_pixels);
			auto light_source_points = emitter_sub_path_buffer.light_source_points.view(0, num_pixels);

			// L0 Rays, for the L0 rays, common_active_pixels is the active_pixels from the camera sub paths.
			build_connection_rays(scene, active_pixels, shading_points, light_source_points, L0_nee_rays);
			occluded(scene, active_pixels, L0_nee_rays, optix_rays, optix_hits); // Check if occuluded.

			sampler->next_bsdf_samples(bsdf_samples);
			bsdf_sample(scene, active_pixels, incoming_rays, incoming_ray_differentials,
				shading_isects, shading_points, bsdf_samples, min_roughness, next_rays, bsdf_ray_differentials, next_min_roughness);

			// Intersect with the scene
			intersect(scene, active_pixels, next_rays, bsdf_ray_differentials, bsdf_isects, bsdf_points, next_ray_differentials, optix_rays, optix_hits);
			////////CN + L0
			accumulate_path_contribs(
				scene,
				active_pixels,
				throughputs,
				incoming_rays,
				shading_isects,
				shading_points,
				light_source_isects,
				light_source_points,
				L0_nee_rays,
				bsdf_isects,
				bsdf_points,
				next_rays,
				min_roughness,
				Real(1) / (options.num_samples* (depth + 1) ),  // MIS , FIX ME. * (depth + 1)
				channel_info,
				next_throughputs,
				rendered_image.get(),
				BufferView<Real>());
			////  py::print("\n camera bounces at:", depth);
			for (int nLight = 0; nLight < light_ray_max_bounces && light_num_active_pixels[nLight] > 0; nLight++) {
				//if((nLight + depth) > max_bounces * 1.5 )
				//  break;
				const auto emitter_active_pixels = emitter_sub_path_buffer.active_pixels.view(nLight * num_pixels, light_num_active_pixels[nLight]);
				const auto light_ray_isects = emitter_sub_path_buffer.light_ray_isects.view(nLight * num_pixels, num_pixels);
				const auto light_ray_points = emitter_sub_path_buffer.light_ray_points.view(nLight * num_pixels, num_pixels);
				const auto light_min_roughness = emitter_sub_path_buffer.min_roughness.view(nLight * num_pixels, num_pixels);
				const auto light_throughputs = emitter_sub_path_buffer.throughputs.view(nLight * num_pixels, num_pixels);
				const auto light_incoming_ray = emitter_sub_path_buffer.light_rays.view(nLight * num_pixels, num_pixels);

				auto L1ShadowRays = path_buffer.nee_rays.view((depth + 1) * num_pixels, num_pixels);
				auto common_active_pixels = path_buffer.active_pixels.view(depth * num_pixels, num_pixels);

				//py::print("emitter_active_pixels before intersect", emitter_active_pixels.size());
				//py::print("active pixels before intersect", active_pixels.size());
				intersect_active_pixels(active_pixels, emitter_active_pixels, common_active_pixels, scene.use_gpu);
				//py::print("Depth at : ", depth, "nLight at:", nLight);
				//py::print("common_active_pixels after  intersection", common_active_pixels.size());

				build_connection_rays(scene, common_active_pixels, shading_points, light_ray_points, L1ShadowRays);
				occluded(scene, common_active_pixels, L1ShadowRays, optix_rays, optix_hits);
				// // // CN + L1
				//if(nLight>2)                
				eval_path_conection(
					scene,
					common_active_pixels,
					incoming_rays,
					L1ShadowRays,
					light_incoming_ray,
					shading_isects,
					shading_points,
					min_roughness,
					light_min_roughness,
					light_ray_isects,
					light_ray_points,
					throughputs,
					light_throughputs,
					Real(2) / (options.num_samples * (depth + nLight + 2)), // Fake mis * (depth + nLight + 2)  scene.light_cdf +
					channel_info,
					rendered_image.get()
				);
			}
			// Compute path contribution & update throughput
			// Stream compaction: remove invalid bsdf intersections
			// active_pixels -> next_active_pixels
			update_active_pixels(active_pixels, bsdf_isects, next_active_pixels, scene.use_gpu);
			// Record the number of active pixels for next depth
			camera_num_active_pixels[depth + 1] = next_active_pixels.size();
		}
		/*
			Part 3. d_accumulation stage.
			Our core insights for now: retrace the BDPT path  as several PT paths.
			We assume the same primary path. But different split along the path split.
			for the CameraSubPathBuffer:
		*/
		if (d_rendered_image.get() != nullptr)
		{
			edge_sampler->begin_sample(sample_id);// Keep this for something in future.			
            /// Traverse the path backward for the derivatives. 
			/// Our contribution  --- split at bdpt eval_path !            
			bool first = true;
			for (int depth = max_bounces - 1; depth >= 0; depth--) {
				auto camera_num_actives = camera_num_active_pixels[depth];
				if (camera_num_actives <= 0) { continue; }  // 如果没有 光路到达这一层，那么跳出。    
	            
				auto active_pixels                    = camera_sub_path_buffer.active_pixels.view(depth * num_pixels, camera_num_actives);

	            auto throughputs                              = camera_sub_path_buffer.throughputs.view(depth * num_pixels, num_pixels);
	            auto incoming_rays                         = camera_sub_path_buffer.rays.view(depth * num_pixels, num_pixels);
	            auto incoming_ray_differentials  = camera_sub_path_buffer.ray_differentials.view(depth * num_pixels, num_pixels);
	            	            
	            auto nee_rays                                     = camera_sub_path_buffer.nee_rays.view(depth * num_pixels, num_pixels);	 
	            
	            auto shading_isects                         = camera_sub_path_buffer.shading_isects.view(depth * num_pixels, num_pixels);
	            auto shading_points                        = camera_sub_path_buffer.shading_points.view(depth * num_pixels, num_pixels);

				auto bsdf_samples                           = camera_sub_path_buffer.bsdf_samples.view(depth * num_pixels, num_pixels);
				auto bsdf_ray_differentials           = camera_sub_path_buffer.bsdf_ray_differentials.view(depth * num_pixels, num_pixels); // First used here.
				auto next_rays                                  = camera_sub_path_buffer.rays.view((depth + 1) * num_pixels, num_pixels);
				
				auto bsdf_isects                            = camera_sub_path_buffer.shading_isects.view((depth + 1) * num_pixels, num_pixels);
	            auto bsdf_points                           = camera_sub_path_buffer.shading_points.view((depth + 1) * num_pixels, num_pixels);
	            auto min_roughness                    = camera_sub_path_buffer.min_roughness.view(depth * num_pixels, num_pixels);
				///      
				/* 
					Main difference with PT is we do not Back Propagate  the light influence via. light samples.  (only few)
					split more to the light path .					
				*/
				//         //auto light_samples                    = camera_sub_path_buffer.light_samples.view(depth * num_pixels, num_pixels);
				//         //auto light_isects                     = camera_sub_path_buffer.light_isects.view(depth * num_pixels, num_pixels);
				//         //auto light_points                     = camera_sub_path_buffer.light_points.view(depth * num_pixels, num_pixels);
	            auto d_light_vertices                  = camera_sub_path_buffer.d_light_vertices.view(0, 3 * camera_num_actives);
	            auto d_bsdf_vertices                   = camera_sub_path_buffer.d_bsdf_vertices.view(0, 3 * camera_num_actives);
	            auto d_diffuse_texs                     = camera_sub_path_buffer.d_diffuse_texs.view(0, camera_num_actives);
	            auto d_specular_texs                  = camera_sub_path_buffer.d_specular_texs.view(0, camera_num_actives);
	            auto d_roughness_texs               = camera_sub_path_buffer.d_roughness_texs.view(0, camera_num_actives);
	            auto d_nee_lights                        = camera_sub_path_buffer.d_nee_lights.view(0, camera_num_actives);
	            auto d_bsdf_lights                       = camera_sub_path_buffer.d_bsdf_lights.view(0, camera_num_actives);
	            auto d_envmap_vals                   = camera_sub_path_buffer.d_envmap_vals.view(0, camera_num_actives);
	            auto d_world_to_envs                = camera_sub_path_buffer.d_world_to_envs.view(0, camera_num_actives);

				/*4组用来传递层与层之间数据的d_X buffer */
				auto d_next_throughputs = camera_sub_path_buffer.d_next_throughputs.view(0, num_pixels);
				auto d_next_rays = camera_sub_path_buffer.d_next_rays.view(0, num_pixels);
				auto d_next_ray_differentials = camera_sub_path_buffer.d_next_ray_differentials.view(0, num_pixels);
				auto d_next_points = camera_sub_path_buffer.d_next_points.view(0, num_pixels);

				auto d_throughputs = camera_sub_path_buffer.d_throughputs.view(0, num_pixels);
				auto d_rays = camera_sub_path_buffer.d_rays.view(0, num_pixels);
				auto d_ray_differentials = camera_sub_path_buffer.d_ray_differentials.view(0, num_pixels);
				auto d_points = camera_sub_path_buffer.d_points.view(0, num_pixels);

				if (first) {
					first = false;
					// Initialize the derivatives propagated from the next vertex
					DISPATCH(scene.use_gpu, thrust::fill,	d_next_throughputs.begin(), d_next_throughputs.end(),Vector3{ 0, 0, 0 });
					DISPATCH(scene.use_gpu, thrust::fill,	d_next_rays.begin(), d_next_rays.end(), DRay{});
					DISPATCH(scene.use_gpu, thrust::fill,	d_next_ray_differentials.begin(), d_next_ray_differentials.end(),RayDifferential{ Vector3{0, 0, 0}, Vector3{0, 0, 0},	Vector3{0, 0, 0}, Vector3{0, 0, 0} });
					DISPATCH(scene.use_gpu, thrust::fill,	d_next_points.begin(), d_next_points.end(),	SurfacePoint::zero());
				}

	             //// Backpropagate path contribution, 
			    //// Since we do not change the L0 path contribution at all. So we just reuse the code from Redner. (We probably need to recheck this since BDPT have a very different MIS calculation.)

				auto L0_nee_rays = camera_sub_path_buffer.nee_rays.view(depth * num_pixels, num_pixels);  // nee_rays in PT
				auto light_source_isects = emitter_sub_path_buffer.light_source_isects.view(0, num_pixels);     //  light_isects  in PT 
				auto light_source_points = emitter_sub_path_buffer.light_source_points.view(0, num_pixels);   // light_points in PT

				// L0 Rays, for the L0 rays, common_active_pixels is the active_pixels from the camera sub paths.
				// build_connection_rays(scene, active_pixels, shading_points, light_source_points, L0_nee_rays);			Already build , rebuild make 

			d_accumulate_path_contribs(
	              scene,
	              active_pixels,
	              throughputs,
	              incoming_rays,
	              incoming_ray_differentials,
	              light_samples, bsdf_samples,
	              shading_isects, shading_points,
				  light_source_isects, light_source_points, L0_nee_rays,  // We re do some thing here.
	              bsdf_isects, bsdf_points, next_rays, bsdf_ray_differentials,
	              min_roughness,
	              Real(1) / (options.num_samples * (depth + 1)), // We changed the MIS weight to heustic method. 
	              channel_info,
	              d_rendered_image.get(),
	              d_next_throughputs,
	              d_next_rays,
	              d_next_ray_differentials,
	              d_next_points,
	              d_light_vertices,
	              d_bsdf_vertices,
	              d_diffuse_texs,
	              d_specular_texs,
	              d_roughness_texs,
	              d_nee_lights,
	              d_bsdf_lights,
	              d_envmap_vals,
	              d_world_to_envs,
	              d_throughputs,
	              d_rays,
	              d_ray_differentials,
	              d_points);

			/*
				  edge sampling code here. Since these code are not our main contribution, we  decoment them for now.
			*/

			   for(int nLight = light_ray_max_bounces -1 ; nLight >= 0 ; nLight --)  {
	          //             /*
	          //                 Buffers from emitterSubPath
	          //             */                   
	          //             d_intersection(); 
	          //             d_accumulate_pathCNLN_contribs();
	           }

			// Deposit vertices, texture, light derivatives
			// sort the derivatives by id & reduce by key


			accumulate_vertex(
				d_light_vertices,
				camera_sub_path_buffer.d_vertex_reduce_buffer.view(0, 3 * camera_num_actives),
				d_scene->shapes.view(0, d_scene->shapes.size()),
				scene.use_gpu,
				thrust_alloc);
			accumulate_vertex(
				d_bsdf_vertices,
				camera_sub_path_buffer.d_vertex_reduce_buffer.view(0, 3 * camera_num_actives),
				d_scene->shapes.view(0, d_scene->shapes.size()),
				scene.use_gpu,
				thrust_alloc);

			//accumulate_vertex(
			//	d_edge_vertices,
			//	camera_sub_path_buffer.d_vertex_reduce_buffer.view(0, 2 * camera_num_actives),
			//	d_scene->shapes.view(0, d_scene->shapes.size()),
			//	scene.use_gpu,
			//	thrust_alloc);

			// for (int i = 0; i < active_pixels.size(); i++) {
			//     auto pixel_id = active_pixels[i];
			//     auto d_diffuse_tex = d_diffuse_texs[i];
			//     if (d_diffuse_tex.material_id == 4) {
			//         debug_image[3 * pixel_id + 0] += d_diffuse_tex.t00[0];
			//         debug_image[3 * pixel_id + 1] += d_diffuse_tex.t00[1];
			//         debug_image[3 * pixel_id + 2] += d_diffuse_tex.t00[2];
			//     }
			// }

			accumulate_diffuse(scene,d_diffuse_texs, camera_sub_path_buffer.d_tex3_reduce_buffer.view(0, camera_num_actives),d_scene->materials.view(0, d_scene->materials.size()),	thrust_alloc);
			accumulate_specular(scene,	d_specular_texs, camera_sub_path_buffer.d_tex3_reduce_buffer.view(0, camera_num_actives),d_scene->materials.view(0, d_scene->materials.size()),thrust_alloc);
			
			// for (int i = 0; i < active_pixels.size(); i++) {
			//     auto pixel_id = active_pixels[i];
			//     auto d_roughness_tex = d_roughness_texs[i];
			//     if (d_roughness_tex.material_id == 4) {
			//         debug_image[3 * pixel_id + 0] += d_roughness_tex.t000;
			//         debug_image[3 * pixel_id + 1] += d_roughness_tex.t000;
			//         debug_image[3 * pixel_id + 2] += d_roughness_tex.t000;
			//     }
			// }
			
			accumulate_roughness(scene,	d_roughness_texs, camera_sub_path_buffer.d_tex1_reduce_buffer.view(0, camera_num_actives),d_scene->materials.view(0, d_scene->materials.size()),thrust_alloc);
			accumulate_area_light(d_nee_lights, camera_sub_path_buffer.d_lgt_reduce_buffer.view(0, camera_num_actives),d_scene->area_lights.view(0, d_scene->area_lights.size()),	scene.use_gpu,thrust_alloc);
			accumulate_area_light(d_bsdf_lights, camera_sub_path_buffer.d_lgt_reduce_buffer.view(0, camera_num_actives),	d_scene->area_lights.view(0, d_scene->area_lights.size()),	scene.use_gpu, thrust_alloc);
			
			if (scene.envmap != nullptr) {
				accumulate_envmap(	scene,	d_envmap_vals,	d_world_to_envs, camera_sub_path_buffer.d_envmap_reduce_buffer.view(0, camera_num_actives),	d_scene->envmap,	thrust_alloc);
			}

			// Previous become next
			std::swap(path_buffer.d_next_throughputs, camera_sub_path_buffer.d_throughputs);
			std::swap(path_buffer.d_next_rays, camera_sub_path_buffer.d_rays);
			std::swap(path_buffer.d_next_ray_differentials, camera_sub_path_buffer.d_ray_differentials);
			std::swap(path_buffer.d_next_points, camera_sub_path_buffer.d_points);
			} /* End of camera  path  [bounces]. (decending) */
		} /*End of d_rendered_image */
			   		 
	}

	if (scene.use_gpu) {
		cuda_synchronize();
	}
	channel_info.free();
	parallel_cleanup();
#ifdef __NVCC__
	if (old_device_id != -1) {
		checkCuda(cudaSetDevice(old_device_id));
	}
#endif
}























struct is_valid_density {
	is_valid_density(const int *sample_density, const int idx) :
		sample_density(sample_density), cur_sample_idx(idx) {}
	DEVICE bool operator()(int pixel_id) {
		return sample_density[pixel_id] < cur_sample_idx;
	}
	const int *sample_density;
	int cur_sample_idx;
};

void update_active_pixels_by_density(BufferView<int> &active_pixels,
	const Buffer<int> &density,
	const int cur_sample_idx,
	bool use_gpu) {
	auto op = is_valid_density{ density.begin(),cur_sample_idx };
	auto new_end = DISPATCH(use_gpu, thrust::remove_if,
		active_pixels.begin(), active_pixels.end(),
		active_pixels.begin(), op);
	active_pixels.count = new_end - active_pixels.begin();
}




void test_construct_densitymap(Buffer<int> &density_map, const int w, const int h) {
	auto totalsize = density_map.size();
	auto long_dist = (w * w) / 4 + (h * h) / 4;
	for (auto x = 0; x < w; x++)
		for (auto y = 0; y < h; y++) {
			// auto dist_x_2 = (x - w / 2) * ( x - w/2 );
			// auto dist_y_2 = (y - h / 2) * ( y - h/2 );
			// float factor = 1.0 -  (dist_x_2 + dist_y_2)/long_dist;

			auto dist_x_2 = (x - w / 2) * (x - w / 2);
			auto dist_y_2 = (y - h / 2) * (y - h / 2);
			float factor = (dist_x_2 + dist_y_2) / long_dist;
			density_map[x*h + y] = (int)factor * 32;
			if ((x + y) > w)
				density_map[x*h + y] = (int)32;

			//else 
			//            density_map[x*h+y] = (int)  1;
		}
}

struct create_sampler_map {
	DEVICE void operator()(int idx) {
		weights[idx] = 1.0 / (input_density_image[idx] * max_sample_num);
		density_map[idx] = input_density_image[idx] * max_sample_num;
	}
	const float *input_density_image;
	const int max_sample_num;
	int *density_map;
	float *weights;
};

void r2r_forward_render(const Scene &scene,
	const RenderOptions &options,
	ptr<float> rendered_image,
	ptr<float> d_rendered_image,
	std::shared_ptr<DScene> d_scene,
	ptr<float> debug_image,
	ptr<float> input_density_image)
{
#ifdef __NVCC__
	int old_device_id = -1;
	if (scene.use_gpu) {
		checkCuda(cudaGetDevice(&old_device_id));
		if (scene.gpu_index != -1) {
			checkCuda(cudaSetDevice(scene.gpu_index));
		}
	}
#endif
	parallel_init();
	ChannelInfo channel_info(options.channels, scene.use_gpu);
	// Some common variables
	const auto &camera = scene.camera;
	auto num_pixels = camera.width * camera.height;
	auto max_bounces = options.max_bounces;

	// A main difference between our path tracer and the usual path
	// tracer is that we need to store all the intermediate states
	// for later computation of derivatives.
	// Therefore we allocate a big buffer here for the storage.
	PathBuffer path_buffer(max_bounces, num_pixels, scene.use_gpu, channel_info);
	auto num_active_pixels = std::vector<int>((max_bounces + 1) * num_pixels, 0);

	/*
		TO Check: sampler -> r2rSampler
	*/
	//auto sampler = r2rSampler(scene.use_gpu, options.seed, num_pixels);    
	std::unique_ptr<Sampler> sampler, edge_sampler;
	switch (options.sampler_type)
	{
	case SamplerType::independent: {
		sampler = std::unique_ptr<Sampler>(new PCGSampler(scene.use_gpu, options.seed, num_pixels));
		edge_sampler = std::unique_ptr<Sampler>(
			new PCGSampler(scene.use_gpu, options.seed + 131071U, num_pixels));
		break;
	}
	case SamplerType::sobol: {
		sampler = std::unique_ptr<Sampler>(new SobolSampler(scene.use_gpu, options.seed, num_pixels));
		edge_sampler = std::unique_ptr<Sampler>(
			new SobolSampler(scene.use_gpu, options.seed + 131071U, num_pixels));
		break;
	}
							 // case SamplerType::r2: {
							 //     sampler = std::unique_ptr<Sampler>(new r2rSampler(scene.use_gpu, options.seed, num_pixels));
							 //     edge_sampler = std::unique_ptr<Sampler>(
							 //         new r2rSampler(scene.use_gpu, options.seed + 131071U, num_pixels));
							 //     break;
							 // } 
	default: {
		assert(false);
		break;
	}
	}
	auto optix_rays = path_buffer.optix_rays.view(0, 2 * num_pixels);
	auto optix_hits = path_buffer.optix_hits.view(0, 2 * num_pixels);

	ThrustCachedAllocator thrust_alloc(scene.use_gpu, num_pixels * sizeof(DTexture3));

	// ADA: 
	Buffer<int> density_map = Buffer<int>(scene.use_gpu, num_pixels);
	Buffer<float> weights = Buffer<float>(scene.use_gpu, num_pixels);
	const int max_sample_num = options.num_samples;

	// TODO: don't know why - cudaMem free error. Use cpu code instead.    
	// parallel_for(create_sampler_map{input_density_image.get(),max_sample_num,density_map.begin(),weights.begin()},density_map.size(),scene.use_gpu);    
	for (int i = 0; i < num_pixels; i++)


	{
		weights[i] = 1.0 / (input_density_image[i] * max_sample_num);
		density_map[i] = input_density_image[i] * max_sample_num;
	}
	// KDT->...  

	// For each sample
	for (int sample_id = 0; sample_id < options.num_samples; sample_id++)
	{
		sampler->begin_sample(sample_id);
		// Buffer view for first intersection
		auto throughputs = path_buffer.throughputs.view(0, num_pixels);
		auto camera_samples = path_buffer.camera_samples.view(0, num_pixels);
		auto rays = path_buffer.rays.view(0, num_pixels);
		auto primary_differentials = path_buffer.primary_ray_differentials.view(0, num_pixels);
		auto ray_differentials = path_buffer.ray_differentials.view(0, num_pixels);
		auto shading_isects = path_buffer.shading_isects.view(0, num_pixels);
		auto shading_points = path_buffer.shading_points.view(0, num_pixels);
		auto primary_active_pixels = path_buffer.primary_active_pixels.view(0, num_pixels);
		auto active_pixels = path_buffer.active_pixels.view(0, num_pixels);
		auto min_roughness = path_buffer.min_roughness.view(0, num_pixels);

		// Initialization
		init_paths(throughputs, min_roughness, scene.use_gpu);
		// Generate primary ray samples
		sampler->next_camera_samples(camera_samples);

		sample_primary_rays(camera, camera_samples, rays, primary_differentials, scene.use_gpu);
		// Initialize pixel id
		// 有效的（ray.dir 不等于 零）光路 就 copy 到 primary_active_pixels 里面
		init_active_pixels(rays, primary_active_pixels, scene.use_gpu, thrust_alloc);
		// KDT->...
	   // Update the valid pixels by density map
	   // ADA: 
	   // TODO:
		update_active_pixels_by_density(primary_active_pixels, density_map, sample_id, scene.use_gpu);
		//update_active_pixels(active_pixels, bsdf_isects, next_active_pixels, scene.use_gpu);

		auto num_actives_primary = (int)primary_active_pixels.size();
		// Intersect with the scene
		intersect(scene,
			primary_active_pixels,
			rays,
			primary_differentials,
			shading_isects,
			shading_points,
			ray_differentials,
			optix_rays,
			optix_hits);
		r2r_accumulate_primary_contribs(scene,
			primary_active_pixels,
			throughputs,
			BufferView<Real>(), // channel multipliers
			rays,
			ray_differentials,
			shading_isects,
			shading_points,
			//Real(1) / options.num_samples,
			weights,
			channel_info,
			rendered_image.get(),
			BufferView<Real>()); // edge_contrib
// Stream compaction: remove invalid intersection
// 去除无效的相交判断 (shape_id <0 或者 tri_id < 0)
		update_active_pixels(primary_active_pixels, shading_isects, active_pixels, scene.use_gpu);
		std::fill(num_active_pixels.begin(), num_active_pixels.end(), 0);
		num_active_pixels[0] = active_pixels.size();
		for (int depth = 0; depth < max_bounces &&
			num_active_pixels[depth] > 0; depth++) {
			// Buffer views for this path vertex
			const auto active_pixels =
				path_buffer.active_pixels.view(depth * num_pixels, num_active_pixels[depth]);
			auto light_samples =
				path_buffer.light_samples.view(depth * num_pixels, num_pixels);
			const auto shading_isects = path_buffer.shading_isects.view(
				depth * num_pixels, num_pixels);
			const auto shading_points = path_buffer.shading_points.view(
				depth * num_pixels, num_pixels);
			auto light_isects = path_buffer.light_isects.view(depth * num_pixels, num_pixels);
			auto light_points = path_buffer.light_points.view(depth * num_pixels, num_pixels);
			auto bsdf_samples = path_buffer.bsdf_samples.view(depth * num_pixels, num_pixels);
			auto incoming_rays = path_buffer.rays.view(depth * num_pixels, num_pixels);
			auto incoming_ray_differentials =
				path_buffer.ray_differentials.view(depth * num_pixels, num_pixels);
			auto bsdf_ray_differentials =
				path_buffer.bsdf_ray_differentials.view(depth * num_pixels, num_pixels);
			auto nee_rays = path_buffer.nee_rays.view(depth * num_pixels, num_pixels);
			auto next_rays = path_buffer.rays.view((depth + 1) * num_pixels, num_pixels);
			auto next_ray_differentials =
				path_buffer.ray_differentials.view((depth + 1) * num_pixels, num_pixels);
			auto bsdf_isects =
				path_buffer.shading_isects.view((depth + 1) * num_pixels, num_pixels);
			auto bsdf_points =
				path_buffer.shading_points.view((depth + 1) * num_pixels, num_pixels);
			const auto throughputs =
				path_buffer.throughputs.view(depth * num_pixels, num_pixels);
			auto next_throughputs =
				path_buffer.throughputs.view((depth + 1) * num_pixels, num_pixels);
			auto next_active_pixels =
				path_buffer.active_pixels.view((depth + 1) * num_pixels, num_pixels);
			auto min_roughness = path_buffer.min_roughness.view(depth * num_pixels, num_pixels);
			auto next_min_roughness = path_buffer.min_roughness.view((depth + 1) * num_pixels, num_pixels);

			// Sample points on lights， 光源的采样数或许不需要这么大？看來需要。。
			// 光源 样本 是一个四维数据 1
			sampler->next_light_samples(light_samples);
			sample_point_on_light(scene,
				active_pixels,
				shading_points,
				light_samples,
				light_isects,  ///光源点的shape_id,tri_id
				light_points,  ///光源点的空间坐标
				nee_rays);///shadow rays 采样到光源点 - 要着色的模型表面点
			occluded(scene, active_pixels, nee_rays, optix_rays, optix_hits);

			// Sample directions based on BRDF
			sampler->next_bsdf_samples(bsdf_samples);
			bsdf_sample(scene,
				active_pixels,
				incoming_rays,
				incoming_ray_differentials,
				shading_isects,
				shading_points,
				bsdf_samples,
				min_roughness,
				next_rays,
				bsdf_ray_differentials,
				next_min_roughness);
			// Intersect with the scene
			intersect(scene,
				active_pixels,
				next_rays,
				bsdf_ray_differentials,
				bsdf_isects,
				bsdf_points,
				next_ray_differentials,
				optix_rays,
				optix_hits);

			// Compute path contribution & update throughput
			r2r_accumulate_path_contribs(
				scene,
				active_pixels,
				throughputs,
				incoming_rays,
				shading_isects,
				shading_points,
				light_isects,
				light_points,
				nee_rays,
				bsdf_isects,
				bsdf_points,
				next_rays,
				min_roughness,
				//Real(1) / options.num_samples,
				weights,
				channel_info,
				next_throughputs,
				rendered_image.get(),
				BufferView<Real>());

			// Stream compaction: remove invalid bsdf intersections
			// active_pixels -> next_active_pixels
			update_active_pixels(active_pixels, bsdf_isects, next_active_pixels, scene.use_gpu);

			// Record the number of active pixels for next depth
			num_active_pixels[depth + 1] = next_active_pixels.size();
			// KDT- update 
		}
	}

	if (scene.use_gpu) {
		cuda_synchronize();
	}
	channel_info.free();
	parallel_cleanup();
#ifdef __NVCC__
	if (old_device_id != -1) {
		checkCuda(cudaSetDevice(old_device_id));
	}
#endif
}



void r2r_render(const Scene &scene,
	const RenderOptions &options,
	ptr<float> rendered_image,
	ptr<float> d_rendered_image,
	std::shared_ptr<DScene> d_scene,
	ptr<float> debug_image,
	ptr<float> input_density_image) {
#ifdef __NVCC__
	int old_device_id = -1;
	if (scene.use_gpu) {
		checkCuda(cudaGetDevice(&old_device_id));
		if (scene.gpu_index != -1) {
			checkCuda(cudaSetDevice(scene.gpu_index));
		}
	}
#endif

	//py::print("r2r render function begins...\n");

	parallel_init();
	if (d_rendered_image.get() != nullptr) {
		initialize_ltc_table(scene.use_gpu);
	}
	ChannelInfo channel_info(options.channels, scene.use_gpu);

	// Some common variables
	const auto &camera = scene.camera;
	auto num_pixels = camera.width * camera.height;
	auto max_bounces = options.max_bounces;

	// A main difference between our path tracer and the usual path
	// tracer is that we need to store all the intermediate states
	// for later computation of derivatives.
	// Therefore we allocate a big buffer here for the storage.
	PathBuffer path_buffer(max_bounces, num_pixels, scene.use_gpu, channel_info);
	auto num_active_pixels = std::vector<int>((max_bounces + 1) * num_pixels, 0);
	std::unique_ptr<Sampler> sampler, edge_sampler;
	switch (options.sampler_type) {
	case SamplerType::independent: {
		sampler = std::unique_ptr<Sampler>(new PCGSampler(scene.use_gpu, options.seed, num_pixels));
		edge_sampler = std::unique_ptr<Sampler>(
			new PCGSampler(scene.use_gpu, options.seed + 131071U, num_pixels));
		break;
	} case SamplerType::sobol: {
		sampler = std::unique_ptr<Sampler>(new SobolSampler(scene.use_gpu, options.seed, num_pixels));
		edge_sampler = std::unique_ptr<Sampler>(
			new SobolSampler(scene.use_gpu, options.seed + 131071U, num_pixels));
		break;
	} default: {
		assert(false);
		break;
	}
	}
	auto optix_rays = path_buffer.optix_rays.view(0, 2 * num_pixels);
	auto optix_hits = path_buffer.optix_hits.view(0, 2 * num_pixels);

	ThrustCachedAllocator thrust_alloc(scene.use_gpu, num_pixels * sizeof(DTexture3));


	// ADA: 
	Buffer<int> density_map = Buffer<int>(scene.use_gpu, num_pixels);
	Buffer<float> weights = Buffer<float>(scene.use_gpu, num_pixels);
	const int max_sample_num = options.num_samples;

	// TODO: don't know why - cudaMem free error. Use cpu code instead.    
	// parallel_for(create_sampler_map{input_density_image.get(),max_sample_num,density_map.begin(),weights.begin()},density_map.size(),scene.use_gpu);    
	for (int i = 0; i < num_pixels; i++)
	{
		assert(input_density_image[i] > 0.0);
		assert(input_density_image[i] < 1.00001);
		weights[i] = 1.0 / (input_density_image[i] * max_sample_num);
		density_map[i] = input_density_image[i] * max_sample_num;

		//weights[i] = 1.0 / (max_sample_num);  
		//density_map[i] = max_sample_num;
	}
	// KDT->...

	// For each sample
	for (int sample_id = 0; sample_id < options.num_samples; sample_id++) {
		sampler->begin_sample(sample_id);

		// Buffer view for first intersection
		auto throughputs = path_buffer.throughputs.view(0, num_pixels);
		auto camera_samples = path_buffer.camera_samples.view(0, num_pixels);
		auto rays = path_buffer.rays.view(0, num_pixels);
		auto primary_differentials = path_buffer.primary_ray_differentials.view(0, num_pixels);
		auto ray_differentials = path_buffer.ray_differentials.view(0, num_pixels);
		auto shading_isects = path_buffer.shading_isects.view(0, num_pixels);
		auto shading_points = path_buffer.shading_points.view(0, num_pixels);
		auto primary_active_pixels = path_buffer.primary_active_pixels.view(0, num_pixels);
		auto active_pixels = path_buffer.active_pixels.view(0, num_pixels);
		auto min_roughness = path_buffer.min_roughness.view(0, num_pixels);

		// Initialization
		init_paths(throughputs, min_roughness, scene.use_gpu);
		// Generate primary ray samples
		sampler->next_camera_samples(camera_samples);
		sample_primary_rays(camera, camera_samples, rays, primary_differentials, scene.use_gpu);
		// Initialize pixel id
		init_active_pixels(rays, primary_active_pixels, scene.use_gpu, thrust_alloc);

		// KDT->...
		// Update the valid pixels by density map
		// ADA: 
		// TODO:
		update_active_pixels_by_density(primary_active_pixels, density_map, sample_id, scene.use_gpu);

		auto num_actives_primary = (int)primary_active_pixels.size();
		// Intersect with the scene
		intersect(scene,
			primary_active_pixels,
			rays,
			primary_differentials,
			shading_isects,
			shading_points,
			ray_differentials,
			optix_rays,
			optix_hits);
		r2r_accumulate_primary_contribs(scene,
			primary_active_pixels,
			throughputs,
			BufferView<Real>(), // channel multipliers
			rays,
			ray_differentials,
			shading_isects,
			shading_points,
			//Real(1) / options.num_samples,
			weights,
			channel_info,
			rendered_image.get(),
			BufferView<Real>()); // edge_contrib
// Stream compaction: remove invalid intersection
		update_active_pixels(primary_active_pixels, shading_isects, active_pixels, scene.use_gpu);
		std::fill(num_active_pixels.begin(), num_active_pixels.end(), 0);
		num_active_pixels[0] = active_pixels.size();
		for (int depth = 0; depth < max_bounces &&
			num_active_pixels[depth] > 0; depth++) {
			// Buffer views for this path vertex
			const auto active_pixels =
				path_buffer.active_pixels.view(depth * num_pixels, num_active_pixels[depth]);
			auto light_samples =
				path_buffer.light_samples.view(depth * num_pixels, num_pixels);
			const auto shading_isects = path_buffer.shading_isects.view(
				depth * num_pixels, num_pixels);
			const auto shading_points = path_buffer.shading_points.view(
				depth * num_pixels, num_pixels);
			auto light_isects = path_buffer.light_isects.view(depth * num_pixels, num_pixels);
			auto light_points = path_buffer.light_points.view(depth * num_pixels, num_pixels);
			auto bsdf_samples = path_buffer.bsdf_samples.view(depth * num_pixels, num_pixels);
			auto incoming_rays = path_buffer.rays.view(depth * num_pixels, num_pixels);
			auto incoming_ray_differentials =
				path_buffer.ray_differentials.view(depth * num_pixels, num_pixels);
			auto bsdf_ray_differentials =
				path_buffer.bsdf_ray_differentials.view(depth * num_pixels, num_pixels);
			auto nee_rays = path_buffer.nee_rays.view(depth * num_pixels, num_pixels);
			auto next_rays = path_buffer.rays.view((depth + 1) * num_pixels, num_pixels);
			auto next_ray_differentials =
				path_buffer.ray_differentials.view((depth + 1) * num_pixels, num_pixels);
			auto bsdf_isects =
				path_buffer.shading_isects.view((depth + 1) * num_pixels, num_pixels);
			auto bsdf_points =
				path_buffer.shading_points.view((depth + 1) * num_pixels, num_pixels);
			const auto throughputs =
				path_buffer.throughputs.view(depth * num_pixels, num_pixels);
			auto next_throughputs =
				path_buffer.throughputs.view((depth + 1) * num_pixels, num_pixels);
			auto next_active_pixels =
				path_buffer.active_pixels.view((depth + 1) * num_pixels, num_pixels);
			auto min_roughness = path_buffer.min_roughness.view(depth * num_pixels, num_pixels);
			auto next_min_roughness =
				path_buffer.min_roughness.view((depth + 1) * num_pixels, num_pixels);

			// Sample points on lights
			sampler->next_light_samples(light_samples);
			sample_point_on_light(scene,
				active_pixels,
				shading_points,
				light_samples,
				light_isects,
				light_points,
				nee_rays);
			occluded(scene, active_pixels, nee_rays, optix_rays, optix_hits);

			// Sample directions based on BRDF
			sampler->next_bsdf_samples(bsdf_samples);
			bsdf_sample(scene,
				active_pixels,
				incoming_rays,
				incoming_ray_differentials,
				shading_isects,
				shading_points,
				bsdf_samples,
				min_roughness,
				next_rays,
				bsdf_ray_differentials,
				next_min_roughness);
			// Intersect with the scene
			intersect(scene,
				active_pixels,
				next_rays,
				bsdf_ray_differentials,
				bsdf_isects,
				bsdf_points,
				next_ray_differentials,
				optix_rays,
				optix_hits);

			// Compute path contribution & update throughput
			r2r_accumulate_path_contribs(
				scene,
				active_pixels,
				throughputs,
				incoming_rays,
				shading_isects,
				shading_points,
				light_isects,
				light_points,
				nee_rays,
				bsdf_isects,
				bsdf_points,
				next_rays,
				min_roughness,
				//Real(1) / options.num_samples,
				weights,
				channel_info,
				next_throughputs,
				rendered_image.get(),
				BufferView<Real>());

			// Stream compaction: remove invalid bsdf intersections
			// active_pixels -> next_active_pixels
			update_active_pixels(active_pixels, bsdf_isects, next_active_pixels, scene.use_gpu);

			// Record the number of active pixels for next depth
			num_active_pixels[depth + 1] = next_active_pixels.size();
		}

		if (d_rendered_image.get() != nullptr) {
			edge_sampler->begin_sample(sample_id);

			// Traverse the path backward for the derivatives
			bool first = true;
			for (int depth = max_bounces - 1; depth >= 0; depth--) {
				// Buffer views for this path vertex
				auto num_actives = num_active_pixels[depth];
				if (num_actives <= 0) {
					continue;
				}
				auto active_pixels =
					path_buffer.active_pixels.view(depth * num_pixels, num_actives);
				auto d_next_throughputs = path_buffer.d_next_throughputs.view(0, num_pixels);
				auto d_next_rays = path_buffer.d_next_rays.view(0, num_pixels);
				auto d_next_ray_differentials =
					path_buffer.d_next_ray_differentials.view(0, num_pixels);
				auto d_next_points = path_buffer.d_next_points.view(0, num_pixels);
				auto throughputs = path_buffer.throughputs.view(depth * num_pixels, num_pixels);
				auto incoming_rays = path_buffer.rays.view(depth * num_pixels, num_pixels);
				auto incoming_ray_differentials =
					path_buffer.ray_differentials.view(depth * num_pixels, num_pixels);
				auto next_rays = path_buffer.rays.view((depth + 1) * num_pixels, num_pixels);
				auto bsdf_ray_differentials =
					path_buffer.bsdf_ray_differentials.view(depth * num_pixels, num_pixels);
				auto nee_rays = path_buffer.nee_rays.view(depth * num_pixels, num_pixels);
				auto light_samples = path_buffer.light_samples.view(
					depth * num_pixels, num_pixels);
				auto bsdf_samples = path_buffer.bsdf_samples.view(depth * num_pixels, num_pixels);
				auto shading_isects = path_buffer.shading_isects.view(
					depth * num_pixels, num_pixels);
				auto shading_points = path_buffer.shading_points.view(
					depth * num_pixels, num_pixels);
				auto light_isects =
					path_buffer.light_isects.view(depth * num_pixels, num_pixels);
				auto light_points =
					path_buffer.light_points.view(depth * num_pixels, num_pixels);
				auto bsdf_isects = path_buffer.shading_isects.view(
					(depth + 1) * num_pixels, num_pixels);
				auto bsdf_points = path_buffer.shading_points.view(
					(depth + 1) * num_pixels, num_pixels);
				auto min_roughness =
					path_buffer.min_roughness.view(depth * num_pixels, num_pixels);

				auto d_throughputs = path_buffer.d_throughputs.view(0, num_pixels);
				auto d_rays = path_buffer.d_rays.view(0, num_pixels);
				auto d_ray_differentials = path_buffer.d_ray_differentials.view(0, num_pixels);
				auto d_points = path_buffer.d_points.view(0, num_pixels);

				auto d_light_vertices = path_buffer.d_light_vertices.view(0, 3 * num_actives);
				auto d_bsdf_vertices = path_buffer.d_bsdf_vertices.view(0, 3 * num_actives);
				auto d_diffuse_texs = path_buffer.d_diffuse_texs.view(0, num_actives);
				auto d_specular_texs = path_buffer.d_specular_texs.view(0, num_actives);
				auto d_roughness_texs = path_buffer.d_roughness_texs.view(0, num_actives);
				auto d_nee_lights = path_buffer.d_nee_lights.view(0, num_actives);
				auto d_bsdf_lights = path_buffer.d_bsdf_lights.view(0, num_actives);
				auto d_envmap_vals = path_buffer.d_envmap_vals.view(0, num_actives);
				auto d_world_to_envs = path_buffer.d_world_to_envs.view(0, num_actives);

				if (first) {
					first = false;
					// Initialize the derivatives propagated from the next vertex
					DISPATCH(scene.use_gpu, thrust::fill,
						d_next_throughputs.begin(), d_next_throughputs.end(),
						Vector3{ 0, 0, 0 });
					DISPATCH(scene.use_gpu, thrust::fill,
						d_next_rays.begin(), d_next_rays.end(), DRay{});
					DISPATCH(scene.use_gpu, thrust::fill,
						d_next_ray_differentials.begin(), d_next_ray_differentials.end(),
						RayDifferential{ Vector3{0, 0, 0}, Vector3{0, 0, 0},
										Vector3{0, 0, 0}, Vector3{0, 0, 0} });
					DISPATCH(scene.use_gpu, thrust::fill,
						d_next_points.begin(), d_next_points.end(),
						SurfacePoint::zero());
				}

				// Backpropagate path contribution
				r2r_d_accumulate_path_contribs(
					scene,
					active_pixels,
					throughputs,
					incoming_rays,
					incoming_ray_differentials,
					light_samples, bsdf_samples,
					shading_isects, shading_points,
					light_isects, light_points, nee_rays,
					bsdf_isects, bsdf_points, next_rays, bsdf_ray_differentials,
					min_roughness,
					//Real(1) / options.num_samples, // weight
					weights,
					channel_info,
					d_rendered_image.get(),
					d_next_throughputs,
					d_next_rays,
					d_next_ray_differentials,
					d_next_points,
					d_light_vertices,
					d_bsdf_vertices,
					d_diffuse_texs,
					d_specular_texs,
					d_roughness_texs,
					d_nee_lights,
					d_bsdf_lights,
					d_envmap_vals,
					d_world_to_envs,
					d_throughputs,
					d_rays,
					d_ray_differentials,
					d_points);

				////////////////////////////////////////////////////////////////////////////////
				// Sample edges for secondary visibility
				auto num_edge_samples = 2 * num_actives;
				auto edge_samples = path_buffer.secondary_edge_samples.view(0, num_actives);
				edge_sampler->next_secondary_edge_samples(edge_samples);
				auto edge_records = path_buffer.secondary_edge_records.view(0, num_actives);
				auto edge_rays = path_buffer.edge_rays.view(0, num_edge_samples);
				auto edge_ray_differentials =
					path_buffer.edge_ray_differentials.view(0, num_edge_samples);
				auto edge_throughputs = path_buffer.edge_throughputs.view(0, num_edge_samples);
				auto edge_shading_isects =
					path_buffer.edge_shading_isects.view(0, num_edge_samples);
				auto edge_shading_points =
					path_buffer.edge_shading_points.view(0, num_edge_samples);
				auto edge_min_roughness =
					path_buffer.edge_min_roughness.view(0, num_edge_samples);
				sample_secondary_edges(
					scene,
					active_pixels,
					edge_samples,
					incoming_rays,
					incoming_ray_differentials,
					shading_isects,
					shading_points,
					nee_rays,
					light_isects,
					light_points,
					throughputs,
					min_roughness,
					d_rendered_image.get(),
					channel_info,
					edge_records,
					edge_rays,
					edge_ray_differentials,
					edge_throughputs,
					edge_min_roughness);

				// Now we path trace these edges
				auto edge_active_pixels = path_buffer.edge_active_pixels.view(0, num_edge_samples);
				init_active_pixels(edge_rays, edge_active_pixels, scene.use_gpu, thrust_alloc);
				// Intersect with the scene
				intersect(scene,
					edge_active_pixels,
					edge_rays,
					edge_ray_differentials,
					edge_shading_isects,
					edge_shading_points,
					edge_ray_differentials,
					optix_rays,
					optix_hits);
				// Update edge throughputs: take geometry terms and Jacobians into account
				update_secondary_edge_weights(scene,
					active_pixels,
					shading_points,
					edge_shading_isects,
					edge_shading_points,
					edge_records,
					edge_throughputs);
				// Initialize edge contribution
				auto edge_contribs = path_buffer.edge_contribs.view(0, num_edge_samples);
				DISPATCH(scene.use_gpu, thrust::fill,
					edge_contribs.begin(), edge_contribs.end(), 0);
				accumulate_primary_contribs(
					scene,
					edge_active_pixels,
					edge_throughputs,
					BufferView<Real>(), // channel multipliers
					edge_rays,
					edge_ray_differentials,
					edge_shading_isects,
					edge_shading_points,
					Real(1) / options.num_samples,
					channel_info,
					nullptr,
					edge_contribs);
				// Stream compaction: remove invalid intersections
				update_active_pixels(edge_active_pixels,
					edge_shading_isects,
					edge_active_pixels,
					scene.use_gpu);
				auto num_active_edge_samples = edge_active_pixels.size();
				// Record the hit points for derivatives computation later
				auto edge_surface_points =
					path_buffer.edge_surface_points.view(0, num_active_edge_samples);
				parallel_for(get_position{
					edge_active_pixels.begin(),
					edge_shading_points.begin(),
					edge_surface_points.begin() }, num_active_edge_samples, scene.use_gpu);
				for (int edge_depth = depth + 1; edge_depth < max_bounces &&
					num_active_edge_samples > 0; edge_depth++) {
					// Path tracing loop for secondary edges
					auto edge_depth_ = edge_depth - (depth + 1);
					auto main_buffer_beg = (edge_depth_ % 2) * (2 * num_pixels);
					auto next_buffer_beg = ((edge_depth_ + 1) % 2) * (2 * num_pixels);
					const auto active_pixels = path_buffer.edge_active_pixels.view(
						main_buffer_beg, num_active_edge_samples);
					auto light_samples = path_buffer.edge_light_samples.view(0, num_edge_samples);
					auto bsdf_samples = path_buffer.edge_bsdf_samples.view(0, num_edge_samples);
					auto tmp_light_samples = path_buffer.tmp_light_samples.view(0, num_actives);
					auto tmp_bsdf_samples = path_buffer.tmp_bsdf_samples.view(0, num_actives);
					auto shading_isects =
						path_buffer.edge_shading_isects.view(main_buffer_beg, num_edge_samples);
					auto shading_points =
						path_buffer.edge_shading_points.view(main_buffer_beg, num_edge_samples);
					auto light_isects = path_buffer.edge_light_isects.view(0, num_edge_samples);
					auto light_points = path_buffer.edge_light_points.view(0, num_edge_samples);
					auto incoming_rays =
						path_buffer.edge_rays.view(main_buffer_beg, num_edge_samples);
					auto ray_differentials =
						path_buffer.edge_ray_differentials.view(0, num_edge_samples);
					auto nee_rays = path_buffer.edge_nee_rays.view(0, num_edge_samples);
					auto next_rays = path_buffer.edge_rays.view(next_buffer_beg, num_edge_samples);
					auto bsdf_isects = path_buffer.edge_shading_isects.view(
						next_buffer_beg, num_edge_samples);
					auto bsdf_points = path_buffer.edge_shading_points.view(
						next_buffer_beg, num_edge_samples);
					const auto throughputs = path_buffer.edge_throughputs.view(
						main_buffer_beg, num_edge_samples);
					auto next_throughputs = path_buffer.edge_throughputs.view(
						next_buffer_beg, num_edge_samples);
					auto next_active_pixels = path_buffer.edge_active_pixels.view(
						next_buffer_beg, num_edge_samples);
					auto edge_min_roughness =
						path_buffer.edge_min_roughness.view(main_buffer_beg, num_edge_samples);
					auto edge_next_min_roughness =
						path_buffer.edge_min_roughness.view(next_buffer_beg, num_edge_samples);

					// Sample points on lights
					edge_sampler->next_light_samples(tmp_light_samples);
					// Copy the samples
					parallel_for(copy_interleave<LightSample>{
						tmp_light_samples.begin(), light_samples.begin()},
						tmp_light_samples.size(), scene.use_gpu);
					sample_point_on_light(
						scene, active_pixels, shading_points,
						light_samples, light_isects, light_points, nee_rays);
					occluded(scene, active_pixels, nee_rays, optix_rays, optix_hits);

					// Sample directions based on BRDF
					edge_sampler->next_bsdf_samples(tmp_bsdf_samples);
					// Copy the samples
					parallel_for(copy_interleave<BSDFSample>{
						tmp_bsdf_samples.begin(), bsdf_samples.begin()},
						tmp_bsdf_samples.size(), scene.use_gpu);
					bsdf_sample(scene,
						active_pixels,
						incoming_rays,
						ray_differentials,
						shading_isects,
						shading_points,
						bsdf_samples,
						edge_min_roughness,
						next_rays,
						ray_differentials,
						edge_next_min_roughness);
					// Intersect with the scene
					intersect(scene,
						active_pixels,
						next_rays,
						ray_differentials,
						bsdf_isects,
						bsdf_points,
						ray_differentials,
						optix_rays,
						optix_hits);

					// Compute path contribution & update throughput
					accumulate_path_contribs(
						scene,
						active_pixels,
						throughputs,
						incoming_rays,
						shading_isects,
						shading_points,
						light_isects,
						light_points,
						nee_rays,
						bsdf_isects,
						bsdf_points,
						next_rays,
						edge_min_roughness,
						Real(1) / options.num_samples,
						channel_info,
						next_throughputs,
						nullptr,
						edge_contribs);

					// Stream compaction: remove invalid bsdf intersections
					// active_pixels -> next_active_pixels
					update_active_pixels(active_pixels, bsdf_isects,
						next_active_pixels, scene.use_gpu);
					num_active_edge_samples = next_active_pixels.size();
				}
				// Now the path traced contribution for the edges is stored in edge_contribs
				// We'll compute the derivatives w.r.t. three points: two on edges and one on
				// the shading point
				auto d_edge_vertices = path_buffer.d_general_vertices.view(0, num_edge_samples);
				accumulate_secondary_edge_derivatives(scene,
					active_pixels,
					shading_points,
					edge_records,
					edge_surface_points,
					edge_contribs,
					d_points,
					d_edge_vertices);
				////////////////////////////////////////////////////////////////////////////////
				cuda_synchronize();
				for (int i = 0; i < num_actives; i++) {
					auto pixel_id = active_pixels[i];
					// auto d_p = d_points[pixel_id].position;
					// debug_image[3 * pixel_id + 0] += d_p[0];
					// debug_image[3 * pixel_id + 1] += d_p[0];
					// debug_image[3 * pixel_id + 2] += d_p[0];
					auto c = 1;
					auto d_e_v0 = d_edge_vertices[2 * i + 0];
					auto d_e_v1 = d_edge_vertices[2 * i + 1];
					if (d_e_v0.shape_id == 1) {
						auto d_v0 = d_e_v0.d_v;
						auto d_v1 = d_e_v1.d_v;
						debug_image[3 * pixel_id + 0] += d_v0[c] + d_v1[c];
						debug_image[3 * pixel_id + 1] += d_v0[c] + d_v1[c];
						debug_image[3 * pixel_id + 2] += d_v0[c] + d_v1[c];
					}
					auto d_l_v0 = d_light_vertices[3 * i + 0];
					auto d_l_v1 = d_light_vertices[3 * i + 1];
					auto d_l_v2 = d_light_vertices[3 * i + 2];
					if (d_l_v0.shape_id == 6) {
						auto d_v0 = d_l_v0.d_v;
						auto d_v1 = d_l_v1.d_v;
						auto d_v2 = d_l_v2.d_v;
						debug_image[3 * pixel_id + 0] += d_v0[c] + d_v1[c] + d_v2[c];
						debug_image[3 * pixel_id + 1] += d_v0[c] + d_v1[c] + d_v2[c];
						debug_image[3 * pixel_id + 2] += d_v0[c] + d_v1[c] + d_v2[c];
					}
					auto d_b_v0 = d_bsdf_vertices[3 * i + 0];
					auto d_b_v1 = d_bsdf_vertices[3 * i + 1];
					auto d_b_v2 = d_bsdf_vertices[3 * i + 2];
					if (d_b_v0.shape_id == 6) {
						auto d_v0 = d_b_v0.d_v;
						auto d_v1 = d_b_v1.d_v;
						auto d_v2 = d_b_v2.d_v;
						debug_image[3 * pixel_id + 0] += d_v0[c] + d_v1[c] + d_v2[c];
						debug_image[3 * pixel_id + 1] += d_v0[c] + d_v1[c] + d_v2[c];
						debug_image[3 * pixel_id + 2] += d_v0[c] + d_v1[c] + d_v2[c];
					}
				}
				// Deposit vertices, texture, light derivatives
				// sort the derivatives by id & reduce by key
				accumulate_vertex(
					d_light_vertices,
					path_buffer.d_vertex_reduce_buffer.view(0, 3 * num_actives),
					d_scene->shapes.view(0, d_scene->shapes.size()),
					scene.use_gpu,
					thrust_alloc);
				accumulate_vertex(
					d_bsdf_vertices,
					path_buffer.d_vertex_reduce_buffer.view(0, 3 * num_actives),
					d_scene->shapes.view(0, d_scene->shapes.size()),
					scene.use_gpu,
					thrust_alloc);
				accumulate_vertex(
					d_edge_vertices,
					path_buffer.d_vertex_reduce_buffer.view(0, 2 * num_actives),
					d_scene->shapes.view(0, d_scene->shapes.size()),
					scene.use_gpu,
					thrust_alloc);

				// for (int i = 0; i < active_pixels.size(); i++) {
				//     auto pixel_id = active_pixels[i];
				//     auto d_diffuse_tex = d_diffuse_texs[i];
				//     if (d_diffuse_tex.material_id == 4) {
				//         debug_image[3 * pixel_id + 0] += d_diffuse_tex.t00[0];
				//         debug_image[3 * pixel_id + 1] += d_diffuse_tex.t00[1];
				//         debug_image[3 * pixel_id + 2] += d_diffuse_tex.t00[2];
				//     }
				// }

				accumulate_diffuse(
					scene,
					d_diffuse_texs,
					path_buffer.d_tex3_reduce_buffer.view(0, num_actives),
					d_scene->materials.view(0, d_scene->materials.size()),
					thrust_alloc);
				accumulate_specular(
					scene,
					d_specular_texs,
					path_buffer.d_tex3_reduce_buffer.view(0, num_actives),
					d_scene->materials.view(0, d_scene->materials.size()),
					thrust_alloc);

				// for (int i = 0; i < active_pixels.size(); i++) {
				//     auto pixel_id = active_pixels[i];
				//     auto d_roughness_tex = d_roughness_texs[i];
				//     if (d_roughness_tex.material_id == 4) {
				//         debug_image[3 * pixel_id + 0] += d_roughness_tex.t000;
				//         debug_image[3 * pixel_id + 1] += d_roughness_tex.t000;
				//         debug_image[3 * pixel_id + 2] += d_roughness_tex.t000;
				//     }
				// }

				accumulate_roughness(
					scene,
					d_roughness_texs,
					path_buffer.d_tex1_reduce_buffer.view(0, num_actives),
					d_scene->materials.view(0, d_scene->materials.size()),
					thrust_alloc);
				accumulate_area_light(
					d_nee_lights,
					path_buffer.d_lgt_reduce_buffer.view(0, num_actives),
					d_scene->area_lights.view(0, d_scene->area_lights.size()),
					scene.use_gpu,
					thrust_alloc);
				accumulate_area_light(
					d_bsdf_lights,
					path_buffer.d_lgt_reduce_buffer.view(0, num_actives),
					d_scene->area_lights.view(0, d_scene->area_lights.size()),
					scene.use_gpu,
					thrust_alloc);
				if (scene.envmap != nullptr) {
					accumulate_envmap(
						scene,
						d_envmap_vals,
						d_world_to_envs,
						path_buffer.d_envmap_reduce_buffer.view(0, num_actives),
						d_scene->envmap,
						thrust_alloc);
				}

				// Previous become next
				std::swap(path_buffer.d_next_throughputs, path_buffer.d_throughputs);
				std::swap(path_buffer.d_next_rays, path_buffer.d_rays);
				std::swap(path_buffer.d_next_ray_differentials, path_buffer.d_ray_differentials);
				std::swap(path_buffer.d_next_points, path_buffer.d_points);
			}

			// Backpropagate from first vertex to camera
			// Buffer view for first intersection
			if (num_actives_primary > 0) {
				const auto primary_active_pixels =
					path_buffer.primary_active_pixels.view(0, num_actives_primary);
				const auto throughputs = path_buffer.throughputs.view(0, num_pixels);
				const auto camera_samples = path_buffer.camera_samples.view(0, num_pixels);
				const auto rays = path_buffer.rays.view(0, num_pixels);
				const auto primary_ray_differentials =
					path_buffer.primary_ray_differentials.view(0, num_pixels);
				const auto ray_differentials = path_buffer.ray_differentials.view(0, num_pixels);
				const auto shading_isects = path_buffer.shading_isects.view(0, num_pixels);
				const auto shading_points = path_buffer.shading_points.view(0, num_pixels);
				const auto d_rays = path_buffer.d_next_rays.view(0, num_pixels);
				const auto d_ray_differentials =
					path_buffer.d_next_ray_differentials.view(0, num_pixels);
				auto d_points = path_buffer.d_next_points.view(0, num_pixels);
				auto d_direct_lights = path_buffer.d_direct_lights.view(0, num_actives_primary);
				auto d_envmap_vals = path_buffer.d_envmap_vals.view(0, num_actives_primary);
				auto d_world_to_envs = path_buffer.d_world_to_envs.view(0, num_actives_primary);
				auto d_diffuse_texs = path_buffer.d_diffuse_texs.view(0, num_actives_primary);
				auto d_specular_texs = path_buffer.d_specular_texs.view(0, num_actives_primary);
				auto d_roughness_texs = path_buffer.d_roughness_texs.view(0, num_actives_primary);
				auto d_primary_vertices =
					path_buffer.d_general_vertices.view(0, 3 * num_actives_primary);
				auto d_cameras = path_buffer.d_cameras.view(0, num_actives_primary);

				r2r_d_accumulate_primary_contribs(scene,
					primary_active_pixels,
					throughputs,
					BufferView<Real>(), // channel multiplers
					rays,
					ray_differentials,
					shading_isects,
					shading_points,
					//Real(1) / options.num_samples,
					weights,
					channel_info,
					d_rendered_image.get(),
					d_direct_lights,
					d_envmap_vals,
					d_world_to_envs,
					d_rays,
					d_ray_differentials,
					d_points,
					d_diffuse_texs,
					d_specular_texs,
					d_roughness_texs);
				// Propagate to camera
				d_primary_intersection(scene,
					primary_active_pixels,
					camera_samples,
					rays,
					primary_ray_differentials,
					shading_isects,
					d_rays,
					d_ray_differentials,
					d_points,
					d_primary_vertices,
					d_cameras);
				cuda_synchronize();
				for (int i = 0; i < primary_active_pixels.size(); i++) {
					auto c = 1;
					auto pixel_id = primary_active_pixels[i];
					auto d_v0 = d_primary_vertices[3 * i + 0];
					auto d_v1 = d_primary_vertices[3 * i + 1];
					auto d_v2 = d_primary_vertices[3 * i + 2];
					if (d_v0.shape_id == 6) {
						debug_image[3 * pixel_id + 0] += (d_v0.d_v[c] + d_v1.d_v[c] + d_v2.d_v[c]);
						debug_image[3 * pixel_id + 1] += (d_v0.d_v[c] + d_v1.d_v[c] + d_v2.d_v[c]);
						debug_image[3 * pixel_id + 2] += (d_v0.d_v[c] + d_v1.d_v[c] + d_v2.d_v[c]);
					}
				}

				// for (int i = 0; i < primary_active_pixels.size(); i++) {
				 //     auto pixel_id = primary_active_pixels[i];
				 //     auto d_pos = d_cameras[i].position;
				 //     debug_image[3 * pixel_id + 0] += d_pos[0];
				 //     debug_image[3 * pixel_id + 1] += d_pos[0];
				 //     debug_image[3 * pixel_id + 2] += d_pos[0];
				 // } 

				 // Deposit derivatives
				accumulate_area_light(
					d_direct_lights,
					path_buffer.d_lgt_reduce_buffer.view(0, num_actives_primary),
					d_scene->area_lights.view(0, d_scene->area_lights.size()),
					scene.use_gpu,
					thrust_alloc);
				if (scene.envmap != nullptr) {
					accumulate_envmap(
						scene,
						d_envmap_vals,
						d_world_to_envs,
						path_buffer.d_envmap_reduce_buffer.view(0, num_actives_primary),
						d_scene->envmap,
						thrust_alloc);
				}
				accumulate_vertex(
					d_primary_vertices,
					path_buffer.d_vertex_reduce_buffer.view(0, num_actives_primary),
					d_scene->shapes.view(0, d_scene->shapes.size()),
					scene.use_gpu,
					thrust_alloc);
				accumulate_diffuse(
					scene,
					d_diffuse_texs,
					path_buffer.d_tex3_reduce_buffer.view(0, num_actives_primary),
					d_scene->materials.view(0, d_scene->materials.size()),
					thrust_alloc);
				accumulate_specular(
					scene,
					d_specular_texs,
					path_buffer.d_tex3_reduce_buffer.view(0, num_actives_primary),
					d_scene->materials.view(0, d_scene->materials.size()),
					thrust_alloc);
				accumulate_roughness(
					scene,
					d_roughness_texs,
					path_buffer.d_tex1_reduce_buffer.view(0, num_actives_primary),
					d_scene->materials.view(0, d_scene->materials.size()),
					thrust_alloc);

				// Reduce the camera array
				DCameraInst d_camera = DISPATCH_CACHED(scene.use_gpu, thrust_alloc, thrust::reduce,
					d_cameras.begin(), d_cameras.end(), DCameraInst{});
				accumulate_camera(camera, d_camera, d_scene->camera, scene.use_gpu);

			}

			/////////////////////////////////////////////////////////////////////////////////
			// Sample primary edges for geometric derivatives
			if (scene.edge_sampler.edges.size() > 0) {
				auto primary_edge_samples = path_buffer.primary_edge_samples.view(0, num_pixels);
				auto edge_records = path_buffer.primary_edge_records.view(0, num_pixels);
				auto rays = path_buffer.edge_rays.view(0, 2 * num_pixels);
				auto ray_differentials = path_buffer.edge_ray_differentials.view(0, 2 * num_pixels);
				auto throughputs = path_buffer.edge_throughputs.view(0, 2 * num_pixels);
				auto channel_multipliers = path_buffer.channel_multipliers.view(0, 2 * channel_info.num_total_dimensions * num_pixels);
				auto shading_isects = path_buffer.edge_shading_isects.view(0, 2 * num_pixels);
				auto shading_points = path_buffer.edge_shading_points.view(0, 2 * num_pixels);
				auto active_pixels = path_buffer.edge_active_pixels.view(0, 2 * num_pixels);
				auto edge_contribs = path_buffer.edge_contribs.view(0, 2 * num_pixels);
				auto edge_min_roughness = path_buffer.edge_min_roughness.view(0, 2 * num_pixels);
				// Initialize edge contribution
				DISPATCH(scene.use_gpu, thrust::fill, edge_contribs.begin(), edge_contribs.end(), 0);
				// Initialize max roughness
				DISPATCH(scene.use_gpu, thrust::fill, edge_min_roughness.begin(), edge_min_roughness.end(), 0);

				// Generate rays & weights for edge sampling
				edge_sampler->next_primary_edge_samples(primary_edge_samples);
				sample_primary_edges(scene,
					primary_edge_samples,
					d_rendered_image.get(),
					channel_info,
					edge_records,
					rays,
					ray_differentials,
					throughputs,
					channel_multipliers);
				// Initialize pixel id
				init_active_pixels(rays, active_pixels, scene.use_gpu, thrust_alloc);

				// KDT->...
				// Update the valid pixels by density map
				// ADA: 
				// TODO: To check why not working with edges
				// update_active_pixels_by_density (active_pixels, density_map, sample_id, scene.use_gpu);

				// Intersect with the sceneCourtyard
				intersect(scene,
					active_pixels,
					rays,
					ray_differentials,
					shading_isects,
					shading_points,
					ray_differentials,
					optix_rays,
					optix_hits);
				update_primary_edge_weights(scene,
					edge_records,
					shading_isects,
					channel_info,
					throughputs,
					channel_multipliers);
				accumulate_primary_contribs(scene,
					active_pixels,
					throughputs,
					channel_multipliers,
					rays,
					ray_differentials,
					shading_isects,
					shading_points,
					Real(1) / options.num_samples,
					//weights,
					channel_info,
					nullptr,
					edge_contribs);
				// Stream compaction: remove invalid intersections
				update_active_pixels(active_pixels, shading_isects, active_pixels, scene.use_gpu);
				auto active_pixels_size = active_pixels.size();
				for (int depth = 0; depth < max_bounces && active_pixels_size > 0; depth++) {
					// Buffer views for this path vertex
					auto main_buffer_beg = (depth % 2) * (2 * num_pixels);
					auto next_buffer_beg = ((depth + 1) % 2) * (2 * num_pixels);
					const auto active_pixels =
						path_buffer.edge_active_pixels.view(main_buffer_beg, active_pixels_size);
					auto light_samples = path_buffer.edge_light_samples.view(0, 2 * num_pixels);
					auto bsdf_samples = path_buffer.edge_bsdf_samples.view(0, 2 * num_pixels);
					auto tmp_light_samples = path_buffer.tmp_light_samples.view(0, num_pixels);
					auto tmp_bsdf_samples = path_buffer.tmp_bsdf_samples.view(0, num_pixels);
					auto shading_isects =
						path_buffer.edge_shading_isects.view(main_buffer_beg, 2 * num_pixels);
					auto shading_points =
						path_buffer.edge_shading_points.view(main_buffer_beg, 2 * num_pixels);
					auto light_isects = path_buffer.edge_light_isects.view(0, 2 * num_pixels);
					auto light_points = path_buffer.edge_light_points.view(0, 2 * num_pixels);
					auto nee_rays = path_buffer.edge_nee_rays.view(0, 2 * num_pixels);
					auto incoming_rays =
						path_buffer.edge_rays.view(main_buffer_beg, 2 * num_pixels);
					auto ray_differentials =
						path_buffer.edge_ray_differentials.view(0, 2 * num_pixels);
					auto next_rays = path_buffer.edge_rays.view(next_buffer_beg, 2 * num_pixels);
					auto bsdf_isects = path_buffer.edge_shading_isects.view(
						next_buffer_beg, 2 * num_pixels);
					auto bsdf_points = path_buffer.edge_shading_points.view(
						next_buffer_beg, 2 * num_pixels);
					const auto throughputs = path_buffer.edge_throughputs.view(
						main_buffer_beg, 2 * num_pixels);
					auto next_throughputs = path_buffer.edge_throughputs.view(
						next_buffer_beg, 2 * num_pixels);
					auto next_active_pixels = path_buffer.edge_active_pixels.view(
						next_buffer_beg, 2 * num_pixels);
					auto edge_min_roughness =
						path_buffer.edge_min_roughness.view(main_buffer_beg, 2 * num_pixels);
					auto edge_next_min_roughness =
						path_buffer.edge_min_roughness.view(next_buffer_beg, 2 * num_pixels);

					// Sample points on lights
					edge_sampler->next_light_samples(tmp_light_samples);
					// Copy the samples
					parallel_for(copy_interleave<LightSample>{
						tmp_light_samples.begin(), light_samples.begin()},
						tmp_light_samples.size(), scene.use_gpu);
					sample_point_on_light(
						scene, active_pixels, shading_points,
						light_samples, light_isects, light_points, nee_rays);
					occluded(scene, active_pixels, nee_rays, optix_rays, optix_hits);

					// Sample directions based on BRDF
					edge_sampler->next_bsdf_samples(tmp_bsdf_samples);
					// Copy the samples
					parallel_for(copy_interleave<BSDFSample>{
						tmp_bsdf_samples.begin(), bsdf_samples.begin()},
						tmp_bsdf_samples.size(), scene.use_gpu);
					bsdf_sample(scene,
						active_pixels,
						incoming_rays,
						ray_differentials,
						shading_isects,
						shading_points,
						bsdf_samples,
						edge_min_roughness,
						next_rays,
						ray_differentials,
						edge_next_min_roughness);
					// Intersect with the scene
					intersect(scene,
						active_pixels,
						next_rays,
						ray_differentials,
						bsdf_isects,
						bsdf_points,
						ray_differentials,
						optix_rays,
						optix_hits);
					// Compute path contribution & update throughput
					accumulate_path_contribs(
						scene,
						active_pixels,
						throughputs,
						incoming_rays,
						shading_isects,
						shading_points,
						light_isects,
						light_points,
						nee_rays,
						bsdf_isects,
						bsdf_points,
						next_rays,
						edge_min_roughness,
						Real(1) / options.num_samples,
						//weights,
						channel_info,
						next_throughputs,
						nullptr,
						edge_contribs);

					// Stream compaction: remove invalid bsdf intersections
					// active_pixels -> next_active_pixels
					update_active_pixels(active_pixels, bsdf_isects,
						next_active_pixels, scene.use_gpu);
					active_pixels_size = next_active_pixels.size();
				}

				// Convert edge contributions to vertex derivatives
				auto d_vertices = path_buffer.d_general_vertices.view(0, 2 * num_pixels);
				auto d_cameras = path_buffer.d_cameras.view(0, num_pixels);
				compute_primary_edge_derivatives(
					scene, edge_records, edge_contribs, d_vertices, d_cameras);


				for (int i = 0; i < edge_records.size(); i++) {
					auto rec = edge_records[i];
					if (rec.edge.shape_id >= 0) {
						auto edge_pt = rec.edge_pt;
						auto xi = int(edge_pt[0] * camera.width);
						auto yi = int(edge_pt[1] * camera.height);
						auto d_v0 = d_vertices[2 * i + 0].d_v;
						auto d_v1 = d_vertices[2 * i + 1].d_v;
						debug_image[3 * (yi * camera.width + xi) + 0] += d_v0[0] + d_v1[0];
						debug_image[3 * (yi * camera.width + xi) + 1] += d_v0[0] + d_v1[0];
						debug_image[3 * (yi * camera.width + xi) + 2] += d_v0[0] + d_v1[0];
					}
				}

				// Deposit vertices
				accumulate_vertex(
					d_vertices,
					path_buffer.d_vertex_reduce_buffer.view(0, d_vertices.size()),
					d_scene->shapes.view(0, d_scene->shapes.size()),
					scene.use_gpu,
					thrust_alloc);

				// Reduce the camera array
				DCameraInst d_camera = DISPATCH_CACHED(scene.use_gpu, thrust_alloc, thrust::reduce,
					d_cameras.begin(), d_cameras.end(), DCameraInst{});
				accumulate_camera(camera, d_camera, d_scene->camera, scene.use_gpu);

				// for (int i = 0; i < edge_records.size(); i++) {
				//     auto rec = edge_records[i];
				//     auto edge_pt = rec.edge_pt;
				//     auto xi = int(edge_pt[0] * camera.width);
				//     auto yi = int(edge_pt[1] * camera.height);
				//     auto d_pos = d_cameras[i].position;
				//     debug_image[3 * (yi * camera.width + xi) + 0] += d_pos[0];
				//     debug_image[3 * (yi * camera.width + xi) + 1] += d_pos[0];
				//     debug_image[3 * (yi * camera.width + xi) + 2] += d_pos[0];
				// }
			}
			/////////////////////////////////////////////////////////////////////////////////
		}
	}

	if (scene.use_gpu) {
		cuda_synchronize();
	}
	channel_info.free();
	parallel_cleanup();

#ifdef __NVCC__
	if (old_device_id != -1) {
		checkCuda(cudaSetDevice(old_device_id));
	}
#endif
}