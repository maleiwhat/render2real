from distutils.core import setup

#setup(name = 'redner',
#            version = '0.0.1',
#            description = 'Redner',
#            packages = ['pyredner'])

setup(name = 'r2r',
        version = '0.0.1',
        description = 'R2r minimal test python code',
        packages = ['pyredner'])
