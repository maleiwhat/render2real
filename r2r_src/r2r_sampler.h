#pragma once

#include "../redner_src/buffer.h"
#include "../redner_src/vector.h"
#include "../redner_src/camera.h"
#include "../redner_src/area_light.h"
#include "../redner_src/material.h"
#include "../redner_src/edge.h"
#include "../redner_src/sampler.h"
struct pcg32_state;
struct r2r_pixel_state
{
    uint64_t state;
    uint64_t inc;    
};

struct r2rSampler :public Sampler
{
    r2rSampler(bool use_gpu, uint64_t seed, int num_pixels);

    void next_camera_samples(BufferView<TCameraSample<float>> samples) override;
    void next_camera_samples(BufferView<TCameraSample<double>> samples) override;
    void next_light_samples(BufferView<TLightSample<float>> samples) override;
    void next_light_samples(BufferView<TLightSample<double>> samples)override ;
    void next_bsdf_samples(BufferView<TBSDFSample<float>> samples)override;
    void next_bsdf_samples(BufferView<TBSDFSample<double>> samples)override;
    void next_primary_edge_samples(BufferView<TPrimaryEdgeSample<float>> samples)override;
    void next_primary_edge_samples(BufferView<TPrimaryEdgeSample<double>> samples)override;
    void next_secondary_edge_samples(BufferView<TSecondaryEdgeSample<float>> samples)override;
    void next_secondary_edge_samples(BufferView<TSecondaryEdgeSample<double>> samples)override;

    bool use_gpu;
    Buffer<pcg32_state> rng_states;

    //Add r2r_sampler_states here;
    Buffer<r2r_pixel_state> r2r_pixel_states;
    int current_sample_id;
    int current_dimmension;

};